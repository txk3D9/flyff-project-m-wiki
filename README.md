# FlyFF Project M Wiki

# Installation

- Checkout this Repository
- create .env file from .env.dist
- run
    - docker-compose up in docker folder
    - docker exec -it fpwm_webserver bash
    - cd ..
    - ./download_extensions.sh
- import database backups or go to http://localhost:8080/mw-config and recreate the database
- run php maintenance/update.php to install extension databases
- create json files from api (optional, only if api json files are not generated. you can make a status check)
  - cd /var/www/import/api_loader
  - php run.php all
run
- import json data and templates and so on into wiki
  - cd /var/www/import
  - php run.php all