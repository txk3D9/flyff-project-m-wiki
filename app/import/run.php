<?php

require_once dirname(__FILE__).'/autoload.php';

ini_set('display_errors', 1);
error_reporting(E_ERROR);

if (!$argv[1]) {
	throw new \Exception('no arg defined');
}
$dataBag = new DataBag();
$groups = [
	'specifications' => [
		'apiSpecificationToClassTemplates' => new ApiSpecificationToClassTemplatesImport($argv[2]),
	],
	'templates' => [
		'template' => new TemplateImport(),
		'css' => new CssImport(),
		'js' => new JsImport(),
	],
	'other' => [
		'page' => new PageImport(),
		'redirect' => new RedirectImport(),
		'tiles' => new TileImport(),
	],
	'cargo' => [
		'recreateCargoTables' => new RecreateCargoTables(),
	],
	'classes' => [
		'item' => new ItemImport(),
		'monster' => new MonsterImport(),
		'class' => new ClassImport(),
		'npc' => new NPCImport(),
		'world' => new WorldImport(),
		'equipmentSet' => new EquipmentSetImport(),
		'skill' => new SkillImport(),
		'partySkill' => new PartySkillImport(),
		'quest' => new QuestImport(),
		'achievement' => new AchievementImport(),
		'karma' => new KarmaImport(),
		'continent' => new ContinentImport(),
	],
	'icons' => [
		'monsterIcons' => new IconImport('monster'),
		'itemIcons' => new IconImport('item'),
		'classIcons' => new IconImport('class'),
		'skillIcons' => new IconImport('skill'),
		'partySkillIcons' => new IconImport('partySkill'),
		'npcIcons' => new IconImport('npc'),
		'elementIcons' => new IconImport('element'),
	],
	'images' => [
		'images' => new ImageImport(),
	],
	'data' => [
		'monsterData' => new MonsterDataImport(),
		'npcData' => new NPCDataImport(),
		'equipmentSetData' => new EquipmentSetDataImport(),
		'itemData' => new ItemDataImport(),
		'skillData' => new SkillDataImport(),
		'worldData' => new WorldDataImport(),
		'continentData' => new ContinentDataImport(),
		'achievementData' => new AchievementDataImport(),
		'questData' => new QuestDataImport(),
	],
];
$classesToRun = [];
foreach ($groups as $groupName => $classes) {
	/**
	 * @var AbstractImport $class
	 */
	foreach ($classes as $className => $class) {
		if ($argv[1] == 'all'
			|| $argv[1] == $groupName
			|| $argv[1] == $className) {
			$class->setCliArguments($argv);
			$classesToRun[] = $class;
		}
	}
}
//find classes to run
if($classesToRun) {
	foreach ($classesToRun as $class) {
		$class->run();
	}
} else {
	echo "no imports were executed!\n";
}
