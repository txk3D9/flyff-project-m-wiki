- TextReplacementUtility still not working :[ -> see TextReplacementUtility for links
    - http://localhost:8080/wiki/Hunting_Monsters_to_recover_Karma_(Quest:8690) ([[Criminal Cardpuppet]])
    - http://localhost:8080/wiki/Get_the_Key_to_escape_Kebaras_Island_(Quest:3134) (ring)
    - http://localhost:8080/wiki/Urgently_looking_for_a_bandage (request, [[Grr Bandages]]) (!@TODO!)
- is naming still ok?
    - http://localhost:8080/wiki/Doridori_Sushi
    - http://localhost:8080/wiki/Successor_of_Hero_-Scroll-_(Quest:9133)]]
    - check especially NPC names bc of the [[]]

- text replacement:
- http://localhost:8080/wiki/Scroll_of_Pet_Revival_Piece_(A_Class)

- tempalte Ability Entity Tables -> skills hinzufügen

- style
    - Classes_Box.html

- update strategy.... xD

- wiki
    - ads
    
- discord
    - !MrRosaPony
        - food aint sorted by lowest heal to highest heal for example
        - also looking at the drop lists of bosses for example 
          the main farm reason should be on the top like ivi weapons at iblis for example
    - Lokomo
        - and i think it would be cool if the skills on a job page would also have a "effects" tab so you can see what skill has what additional effects on first glance
          for example bleeding on flame geyser or stun on stone spike
    - Askio
       -  Recovery: Maybe change Effects column to 2 columns with Type / Value to allow sorting by value.
        Item Upgrading -> Piercing Cards: same as above
          
- nice to have
    - make category and subcategory filterable! (drop table monster)
    - skill tree (mit hover, und man kann die levels angeben)
    - Classes_Box.html -> add a #parameter (does not work?!?!)
    - data structure world -> property "revivalKey" ->  links to lodestar?
    - leaflet map mit ausgabe...
        - world
            - places
            - lodestars
            - continents
                - polygons?!?!
        - item
            - blinkwingTarget (data and output)