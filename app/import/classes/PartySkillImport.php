<?php

class PartySkillImport extends AbstractApiImport {

	function getCategories($item): array {
		return ['Skill', 'Party Skill'];
	}

	function getNamingIdentifier() {
		return NamingUtility::IDENTIFIER_PARTY_SKILL;
	}

	function getExtraTemplateReplacements($item) {
		return [
			['$description'],
			[
				TextReplacementUtility::replaceTextWithLinkMarkers($item->description->en),
			],
		];
	}
}
