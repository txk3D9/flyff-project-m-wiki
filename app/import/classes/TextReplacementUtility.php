<?php

class TextReplacementUtility {

	protected static $textReplacements;
	const TEXT_REPLACEMENT_BLACKLIST = [
		'Will', //item 3904
		'Is' //npc...
	];

	static function replaceTextWithLinkMarkers($text) {
		if ($text) {
			$textReplacements = self::getTextReplacements();
			foreach ($textReplacements as $textReplacement) {
				$text = preg_replace(
					'/\b'.preg_quote($textReplacement->name).'(\b|s)(?=(((?!\]).)*\[)|[^\[\]]*$)/im',
					'[['.$textReplacement->value.']]',
					$text
				);
			}
		}

		return $text;
	}

	/**
	 * @return TextReplacement[]
	 * @throws Exception
	 */
	protected static function getTextReplacements() {
		if (self::$textReplacements === null) {
			$monsterNames = [];
			$itemNames = [];
			$categories = [];
			$subcategories = [];
			$classNames = [];
			$npcNames = [];
			$dataBag = DataBag::getBag();
			foreach ($dataBag->getMonsterData() as $monster) {
				if ($monster->name->en) {
					$monsterNames[$monster->name->en] = new TextReplacement($monster->name->en, $dataBag->getMonsterIdsToNames()[$monster->id]);
				}
			}
			foreach ($dataBag->getItemData() as $item) {
				if ($item->name->en && !array_key_exists($item->name->en, $itemNames)) {
					$itemNames[$item->name->en] = new TextReplacement($item->name->en, $dataBag->getItemIdsToNames()[$item->id]);
				}
				if ($item->category && !array_key_exists($item->category, $categories)) {
					$categories[$item->category] = new TextReplacement(ucfirst($item->category), null);
				}
				if ($item->subcategory && !array_key_exists($item->subcategory, $categories)) {
					$subcategories[$item->subcategory] = new TextReplacement(ucfirst($item->subcategory), null);
				}
			}
			foreach ($dataBag->getClassData() as $class) {
				if ($class->name->en) {
					$classNames[$class->name->en] = new TextReplacement($class->name->en, $dataBag->getClassIdsToNames()[$class->id]);
				}
			}
			foreach ($dataBag->getNPCData() as $npc) {
				if ($npc->name->en) {
					$npcNames[$npc->name->en] = new TextReplacement($npc->name->en, $dataBag->getNPCIdsToNames()[$npc->id]);
				}
			}
			$customWords = [
				new TextReplacement('Quest Item', 'Quest item'),
				new TextReplacement('Quest item'),
				new TextReplacement('quest item'),
				new TextReplacement('NPC'),
				new TextReplacement('Giant'),
				new TextReplacement('HP'),
				new TextReplacement('Health point'),
				new TextReplacement('Health points'),
				new TextReplacement('Karma'),
				new TextReplacement('MP'),
				new TextReplacement('Mana point'),
				new TextReplacement('Mana points'),
				new TextReplacement('FP'),
				new TextReplacement('Fighting point'),
				new TextReplacement('Fighting points'),
			];
			//order of this arrays is important as it determines if it first checks item and than monster names!
			self::$textReplacements = array_merge($itemNames, $monsterNames, $classNames, $npcNames, $customWords, $categories, $subcategories);
			//filter out unwanted replacements from blacklist
			self::$textReplacements = array_filter(self::$textReplacements, function($a) {
				return !in_array($a->name, self::TEXT_REPLACEMENT_BLACKLIST);
			});
			//sort them so the longest replacements come first -> this will help so stuff like Captain [[Aibatt]] wont happen
			usort(self::$textReplacements, function ($a, $b) {
				return strlen($a->name) < strlen($b->name);
			});

		}

		return self::$textReplacements;
	}
}