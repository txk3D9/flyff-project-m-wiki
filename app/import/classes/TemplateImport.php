<?php

class TemplateImport extends AbstractImport {

	function execute() {
		$wikiTemplatesDir =HelperUtility::getImportFolderPath().'/templates/wiki/';
		$files = array_diff(scandir($wikiTemplatesDir), array('.', '..'));
		$cliArguments = $this->cliArguments;
		unset($cliArguments[0], $cliArguments[1]);
		foreach ($files as $file) {
			if($cliArguments && !in_array($file, $cliArguments)) {
				continue;
			}

			$templateName = "Template:".str_replace(" ", "_", current(explode(".", $file)));
			if ($this->debugOutput) {
				echo "Import Template: ".$templateName;
			}
			MediaWikiUtility::saveFileToPage($templateName, $wikiTemplatesDir.$file);
			if ($this->debugOutput) {
				echo " - done\n";
			}
		}
	}
}

