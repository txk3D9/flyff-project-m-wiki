<?php

class ApiSpecificationToClassTemplatesImport extends AbstractImport {

	protected $oasc;
	protected $unobjectifiedProperties = ['element'];
	protected $handledAsString = ['nameColor', 'color', 'name'];
	protected $handledAsText = ['description', 'descriptionComplete',
								//quest
								'descriptionComplete', 'dialogsBegin',
								'dialogsAccept', 'dialogsDecline',
								'dialogsComplete', 'dialogsFail'
	];
	protected $notRequiredFields = ['name', 'description'];
	protected $handledAsPageName = ['class', 'mainItem', 'transy', 'parent', 'world', 'continent',
									'revivalWorld', 'skill', 'item', 'equipmentSet',
									//quests
									'beginNPC', 'endNPC', 'endTalkNPC',
									//achievements
									'mainMonster', 'mainItem', 'mainSkill', 'mainClass',
	];
	protected $handledAsListOfPages = ['summoned', 'parts', 'beginClasses',
									   //achievements
									   'monsters', 'items', 'skills', 'classes'
	];
	protected $blacklistedProperties = [
	];
	protected $handledAsRelationProperties = ['experienceTable'];
	protected $schemaToTemplateName = [
		'Spawn' => 'Spawn',
		'Location' => 'Location',
		'Karma' => 'Karma_Row',
		'Ability' => 'Ability',
	];
	protected $extraProperties = [
		'Ability' => [
			'equipmentSet' => 'Page',
			'equippedItemCount' => 'Integer',
			'item' => 'Page',
			'inherited' => 'Boolean',
			//skill and SkillLevel will be used to determine relation between skills and abilities.
			// so skill and skillLevel from the api are named skillChanceSkill and skillChanceLevel in the wiki
			'skillChanceSkill' => 'Page',
			'skillChanceLevel' => 'Integer',
		],
		'Achievement' => [
			'shortDescription' => 'String',
		],
		'Continent' => [
			'shortDescription' => 'String',
			'image' => 'File',
		],
		'Class' => [
			'shortDescription' => 'String',
			'image' => 'File',
		],
		'Equipment_Set' => [
			'shortDescription' => 'String',
			'image' => 'File',
			'class' => 'Page',
			'level' => 'Integer',
			'sex' => 'String'
		],
		'Item' => [
			'shortDescription' => 'String',
			'image' => 'File',
			'equipmentSet' => 'Page',
			'hasAbilities' => 'Boolean',
			'flightSpeed' => 'Integer'
		],
		'Location' => [
			'identifier' => 'Page', //f.e. item, monster, npc, world
			'placeType' => 'String', // world -> places
			'lodestarKey' => 'String', // world -> lodestars
		],
		'Monster' => [
			'shortDescription' => 'String',
			'image' => 'File',
		],
		'NPC' => [
			'shortDescription' => 'String',
			'icon' => 'File',
		],
		'Party_Skill' => [
			'shortDescription' => 'String',
		],
		'Quest' => [
			'shortDescription' => 'String',
			'hasItemReward' => 'Boolean',
			'hasEndVisitPlace' => 'Boolean',
			'endReceiveExperience' => 'Float'
		],
		'Spawn' => [
//			'item' => 'Page',
//			'monster' => 'Page',
		],
		'Skill' => [
			'shortDescription' => 'String',
			'image' => 'File',
		],
		'World' => [
			'shortDescription' => 'String',
			'image' => 'File',
		],
	];
	protected $schemasToIgnore = ['Color', 'Language'];
	protected $whitelistedSchema = null;

	public function __construct($whitelistedSchema = null) {
		$this->oasc = $this->getOASC();
		$this->whitelistedSchema = $whitelistedSchema;
	}

	public function execute() {
		foreach ($this->oasc->components->schemas as $schemaName => $schema) {
			if ($schemaName == 'EquipSet') {
				$schemaName = 'Equipment_Set';
			} else if ($schemaName == 'PartySkill') {
				$schemaName = 'Party_Skill';
			}
			if (!$schema->properties) {
				continue;
			}
			if (in_array($schemaName, $this->schemasToIgnore)
				|| $this->whitelistedSchema !== null && $schemaName != ucfirst($this->whitelistedSchema)) {
				continue;
			}
			if (array_key_exists($schemaName, $this->schemaToTemplateName)) {
				$templateName = $this->schemaToTemplateName[$schemaName];
			} else {
				$templateName = $schemaName.'_Infobox';
			}
			if ($this->debugOutput) {
				echo "Import Schema: ".$templateName;
			}
			$tableName = NamingUtility::getTableNameFromSchemaName($schemaName);
			$templateParams = [];
			$declare = [];
			$store = [];
			if (array_key_exists($schemaName, $this->extraProperties)) {
				foreach ($this->extraProperties[$schemaName] as $name => $type) {
					$object = new stdClass();
					$object->type = $type;
					$schema->properties->$name = $object;
				}
			}
			foreach ($schema->properties as $propertyName => $fields) {
				if (
					(in_array($propertyName, $this->blacklistedProperties)
						|| in_array($propertyName, $this->handledAsRelationProperties)
						|| property_exists($fields, 'type') && $fields->type == 'array' && $fields->items->type == 'object' //monster -> drops, attacks
						|| property_exists($fields, 'type') && $fields->type == 'array' && array_key_exists('$ref', get_object_vars($fields->items)) //monster ->spawns
						|| is_array(get_object_vars($fields))
					)
					&& array_key_exists('$ref', get_object_vars($fields))
					&& !in_array($propertyName, $this->unobjectifiedProperties)
					&& !in_array($propertyName, $this->handledAsListOfPages)
					&& !in_array($propertyName, $this->handledAsString)
					&& !in_array($propertyName, $this->handledAsText)
				) {
					continue;
				}
				$templateParams[] = ($propertyName == 'icon') ? 'icon (namespace=File)' : $propertyName;
				$declare[] = $this->getDeclareString($propertyName, $fields);
				$store[] = $this->getStoreString($propertyName, $this->getRequiredFields($schema->required));
			}
			$pageName = 'Template:'.$templateName;
			$file = $this->fillTemplate($templateName, $templateParams, $tableName, $declare, $store);
			MediaWikiUtility::saveFileToPage($pageName, $file);
			MediaWikiUtility::recreateCargoTable($tableName);
			if ($this->debugOutput) {
				echo " - done\n";
			}
			if (!$this->keepTempFiles) {
				unlink($file);
			}
		}
	}

	function getRequiredFields($requiredFieldsBySchema) {
		return array_diff($requiredFieldsBySchema, $this->notRequiredFields);
	}

	function getOASC() {
		$file = HelperUtility::getImportFolderPath().'/api_json_files/openapi_specification.json';
		if (!file_exists($file)) {
			throw new \Exception('file '.$file.' does not exist');
		}

		return json_decode(file_get_contents($file));
	}

	function getDeclareString($propertyName, $fields) {
		if (is_array($this->unobjectifiedProperties) && in_array($propertyName, $this->unobjectifiedProperties)
			|| is_array($this->handledAsString) && in_array($propertyName, $this->handledAsString)) {
			$type = 'String';
		} else if (is_array($this->unobjectifiedProperties) && in_array($propertyName, $this->unobjectifiedProperties)
			|| is_array($this->handledAsText) && in_array($propertyName, $this->handledAsText)) {
			$type = 'Text';
		} else if (is_array($this->handledAsListOfPages) && in_array($propertyName, $this->handledAsListOfPages)) {
			$type = "List (,) of Page";
		} else if (is_array($this->handledAsPageName) && in_array($propertyName, $this->handledAsPageName)) {
			$type = "Page";
		} else if ($fields->type == "array" && $fields->items->type == "number") {
			$type = "List (,) of Float";
		} else if ($fields->type == "array" && $fields->items->type == "integer") {
			$type = "List (,) of Integer";
		} else {
			$type = $fields->type == 'number' ? 'Float' : ucfirst($fields->type);
		}
		$typeAddition = '';
		if (property_exists($fields, 'enum') && $fields->enum) {
			$typeAddition = ' (allowed values='.implode(",", $fields->enum).')';
		} else if ($propertyName == 'element') {
			$typeAddition = ' (allowed values=fire,water,electricity,wind,earth,none)';
		}

		return NamingUtility::getDBFriendlyPropertyName($propertyName).'='.$type.$typeAddition;
	}

	function getStoreString($propertyName, $requiredFields) {
		$requiredPipe = in_array($propertyName, $requiredFields) ? '' : '|';

		return NamingUtility::getDBFriendlyPropertyName($propertyName).'={{{'.$propertyName.$requiredPipe.'}}}';
	}

	function fillTemplate(string $templateName, array $templateParams, string $tableName, array $declare, array $store) {
		$newLine = "\n\t|";
		$templateClass = file_get_contents(HelperUtility::getImportFolderPath().'/templates/cargo_definition.html');
		$templateContent = file_get_contents(HelperUtility::getImportFolderPath().'/templates/classes/'.$templateName.'.html');
		$filedTemplate = str_replace(
			['$content', '$templateParams', '$tableName', '$declare', '$store', '$category'],
			[
				$templateContent,
				implode($newLine, $templateParams),
				$tableName,
				"|".implode($newLine, $declare),
				"|".implode($newLine, $store),
			],
			$templateClass
		);
		$filename = HelperUtility::getImportFolderPath().'/temp/'.$tableName.".html";
		file_put_contents($filename, $filedTemplate);

		return $filename;
	}
}

//open api specification content


