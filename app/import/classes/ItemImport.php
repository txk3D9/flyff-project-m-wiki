<?php

class ItemImport extends AbstractApiImport {

	protected $specialyHandledProperties = ['abilities', 'spawns', 'location'];
	protected $apiValueToWikiNameProperties = ['attackSpeed', 'sex', 'element', 'subcategory', 'category', 'rarity'];

	function getCategories($item): array {
		$categories = ['Item'];
		if ($item->category) {
			$categories[] = NamingUtility::transformApiValueToWikiName($item->category);
		}
		if ($item->subcategory) {
			$categories[] = NamingUtility::transformApiValueToWikiName($item->subcategory);
		}
		if ($item->premium) {
			$categories[] = 'Premium';
		}

		return $categories;
	}

	function handleProperty($propertyName, $propertyValue, $item){
		if($propertyName == 'class'){
			return DataBag::getBag()->getClassIdsToNames()[$propertyValue];
		}
		if($propertyName == 'transy'){
			return DataBag::getBag()->getItemIdsToNames()[$propertyValue];
		}
		if($propertyName == 'category' && $propertyValue == 'quest'){
			return 'Quest item';
		}

		return parent::handleProperty($propertyName, $propertyValue, $item);
	}

	function additionalProperties($item): array {
		$equipmentSetName = null;
		$equipmentSet = DataBag::getBag()->getItemToEquipmentSetIds()[$item->id];
		if($equipmentSet) {
			$equipmentSetName = DataBag::getBag()->getEquipmentSetIdsToNames()[$equipmentSet->id];
		}
		$speed = null;
		if($item->category == 'flying') {
			if($item->id == 6832) {
				$speed = 51;
			} else if ($item->id == 8883) {
				$speed = 33;
			}else{
				$speed = (int) preg_replace("/[^0-9]/", "", $item->description->en);
			}
		}

		return [
			'hasAbilities' => ($item->abilities) ? 1 : 0,
			'equipmentSet' => $equipmentSetName,
			'flightSpeed' => $speed
		];
	}

	function getExtraTemplateReplacements($item) {
		return [
			[
//				'$dropTable', '$soldByTable',
				'$description'
			],
			[
//				(array_key_exists($item->id, DataBag::getBag()->getItemIdsDroppedByMonsters()))
//					? '{{Item Drop Table}}'
//					: '',
//				(array_key_exists($item->id, DataBag::getBag()->getItemIdsSoldByNPCs()))
//					? '{{Item Sold By Table}}'
//					: '',
			$item->description->en && $item->description->en != "null"
				? TextReplacementUtility::replaceTextWithLinkMarkers($item->description->en) : '',
			],
		];
	}
}
