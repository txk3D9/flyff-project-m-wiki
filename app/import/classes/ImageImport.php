<?php

class ImageImport extends AbstractImport {

	function execute() {
		$imageDir = HelperUtility::getImportFolderPath().'images/';
		if(file_exists($imageDir)) {
			if($this->debugOutput) {
				echo "start import images\n";
			}
			MediaWikiUtility::importImageFromFolder($imageDir);
			if($this->debugOutput) {
				echo "end import images\n";
			}
		} else {
			throw new \Exception("Icon dir for ".$this->identifier." does not exist: ".$imageDir);
		}
	}
}