<?php

class CssImport extends AbstractImport {

	function execute() {
		$cssFile = HelperUtility::getImportFolderPath().'/css/common.css';
		MediaWikiUtility::saveFileToPage("MediaWiki:Common.css", $cssFile);
	}
}
