<?php

class AchievementDataImport extends AbstractDataImport {

	protected function getData() {
		return DataBag::getBag()->getAchievementData();
	}

	protected function getNamingIdentifier() {
		return NamingUtility::IDENTIFIER_ACHIEVEMENT;
	}

	protected function getContent($achievement) {
		$contents = [];
		$levelRows = $this->getLevelRows($achievement);
		if ($levelRows) {
			$contents[] = "== Achievement Levels ==\n\n".implode("\n", $levelRows);
		}
		$levelItemRows = $this->getLevelItemRows($achievement);
		if ($levelItemRows) {
			$contents[] = "== Achievement Level Items ==\n\n".implode("\n", $levelItemRows);
		}
		if ($contents) {
			return parent::getContent($achievement).implode("\n\n", $contents);
		}
	}

	protected function getLevelRows($achievement) {
		$rows = [];
		if ($achievement->levels) {
			foreach ($achievement->levels as $num => $achievementLevel) {
				$rows[] = '{{Achievement Level|'.implode("|", [
						'achievement={{PAGENAME}}',
						'value='.$achievementLevel->value,
						'level='.($num + 1),
						'name='.$achievementLevel->name->en,
						'title='.$achievementLevel->title->en,
						'attackPower='.$achievementLevel->attackPower,
						'inventorySpace='.$achievementLevel->inventorySpace,
						'gold='.$achievementLevel->gold,
						'hasItems='.($achievementLevel->items ? 1 : 0),
					]).'}}';
			}
		}

		return $rows;
	}

	protected function getLevelItemRows($achievement) {
		$rows = [];
		if ($achievement->levels) {
			foreach ($achievement->levels as $num => $achievementLevel) {
				if($achievementLevel->items) {
					foreach($achievementLevel->items as $item) {
						$rows[] = '{{Achievement Level Item|'.implode("|", [
								'achievement={{PAGENAME}}',
								'level='.($num + 1),
								'item='.DataBag::getBag()->getItemIdsToNames()[$item->item],
								'count='.$item->count,
								'soulLinked='.($item->soulLinked ? 1 : 0),
							]).'}}';
					}
				}
			}
		}

		return $rows;
	}
}