<?php

class ItemDataImport extends AbstractDataImport {

	use LocationImport;
	use SpawnImport;
	use AbilityImport;

	protected function getData() {
		return DataBag::getBag()->getItemData();
	}
	protected function getNamingIdentifier() {
		return NamingUtility::IDENTIFIER_ITEM;
	}
	protected function getContent($item) {
		$contents = [];
		$abilityRows = $this->getAbilityRows($item);
		if ($abilityRows) {
			$contents[] = "== Abilities ==\n\n".implode("\n", $abilityRows);
		}
		$spawnRows = $this->getSpawnRows($item->spawns);
		if ($spawnRows) {
			$contents[] = "== Spawns ==\n\n".implode("\n", $spawnRows);
		}
		$locationRows = $this->getLocationRows($item->location);
		if ($locationRows) {
			$contents[] = "== Location ==\n\n".implode("\n", $locationRows);
		}
		if ($contents) {
			return parent::getContent($item).implode("\n\n", $contents);
		}
	}
}