<?php

class WorldDataImport extends AbstractDataImport {

	use LocationImport;

	protected function getData() {
		return DataBag::getBag()->getWorldData();
	}

	protected function getNamingIdentifier() {
		return NamingUtility::IDENTIFIER_WORLD;
	}

	protected function getContent($world) {
		$contents = [];
		$placesRows = $this->getPlacesRows($world);
		if ($placesRows) {
			$contents[] = "== Places ==\n\n".implode("\n", $placesRows);
		}
		$lodestarRows = $this->getLodestarRows($world);
		if ($lodestarRows) {
			$contents[] = "== Lodestars ==\n\n".implode("\n", $lodestarRows);
		}
		if ($contents) {
			return parent::getContent($world).implode("\n\n", $contents);
		}
	}

	private function getPlacesRows($world) {
		$rows = [];
		if ($world->places) {
			foreach ($world->places as $place) {
				$rows = array_merge($rows, $this->getLocationRows($place->location, ['placeType='.
																		   NamingUtility::transformApiValueToWikiName($place->type)]));
			}
		}

		return $rows;
	}

	private function getLodestarRows($world) {
		$rows = [];
		if ($world->lodestars) {
			foreach ($world->lodestars as $lodestar) {
				$rows = array_merge($rows, $this->getLocationRows($lodestar->location, ['lodestarKey='.$lodestar->key]));
			}
		}

		return $rows;
	}
}