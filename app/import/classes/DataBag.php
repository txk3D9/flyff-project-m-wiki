<?php

class DataBag {

	const IMPORT_VERSION = 0.6;
	public static $bag = null;
	protected $itemIdsToNames = null;
	protected $equipmentSetIdsToNames = null;
	protected $skillIdsToNames = null;
	protected $classIdsToNames = null;
	protected $monsterIdsToNames = null;
	protected $npcIdsToNames = NULL;
	protected $questIdsToNames = NULL;
	protected $worldIdsToNames = null;
	protected $continentIdsToNames = null;
	protected $itemToEquipmentSetIds = null;
	protected $itemIdsDroppedByMonsters = null;
	protected $itemIdsSoldByNPCs = null;
	protected $textReplacements = null;
	protected $duplicateNames = null;
//	protected $armorItemCounterparts = null;
//	protected $fashionItemCounterparts = null;
	protected $apiJsonData = [];
	protected $continentData = [];

	/**
	 * @return DataBag
	 */
	public static function getBag() {
		if (DataBag::$bag === null) {
			DataBag::$bag = new DataBag();
		}

		return DataBag::$bag;
	}

	protected function getAbilityParameters() {
		$file = HelperUtility::getImportFolderPath().'/api_json_files/openapi_specification.json';
		$content = json_decode(file_get_contents($file));

		return array_unique($content->components->schemas->Ability->properties->parameter->enum);
	}

	public function getApiJsonData($identifier): array {
		if ($identifier == 'equipmentset') {
			$identifier = 'equipset';
		} else if ($identifier == 'continent') {
			return $this->getContinentData();
		} else if ($identifier == 'abilityParameters') {
			return $this->getAbilityParameters();
		}
		if (!array_key_exists($identifier, $this->apiJsonData)) {
			$fileName = $identifier.'.json';
			$file = HelperUtility::getImportFolderPath().'api_json_files/'.$fileName;
			if (!file_exists($file)) {
				throw new \Exception('file '.$file.' not found');
			}
			$data = [];
			//set the id of the item as array key
			foreach (json_decode(file_get_contents($file)) as $d) {
				$data[$d->id] = $d;
			}
			$this->apiJsonData[$identifier] = $data;
		}

		return $this->apiJsonData[$identifier];
	}

	public function getItemToEquipmentSetIds() {
		if ($this->itemToEquipmentSetIds == null) {
			foreach ($this->getEquipmentSetData() as $eqSet) {
				if ($eqSet->parts) {
					foreach ($eqSet->parts as $partId) {
						$this->itemToEquipmentSetIds[$partId] = $eqSet;
					}
				}
			}
		}

		return $this->itemToEquipmentSetIds;
	}

//	public function getArmorItemCounterParts() {
//		if ($this->armorItemCounterparts == null) {
//			$filteredItems = array_filter($this->getItemData(), function ($item) {
//				return $item->category == "armor";
//			});
//			foreach ($filteredItems as $item) {
//				$counterparts = $this->findArmorItemCounterparts($filteredItems, $item);
//				if (count($counterparts) == 1) {
//					$this->armorItemCounterparts[$item->id] = $counterparts[0];
//				} else if (count($counterparts) > 1) { //multiple counter parts found (bc there are multiple 105 sets)
//					$eqSetOfItem = $this->getItemToEquipmentSetIds()[$item->id];
//					foreach ($counterparts as $counterpart) {
//						$counterpartSet = $this->getItemToEquipmentSetIds()[$counterpart->id];
//						if ($eqSetOfItem->bonus == $counterpartSet->bonus) {
//							$this->armorItemCounterparts[$item->id] = $counterpart;
//							continue 2;
//						}
//					}
//					throw new \Exception('multiple counterparts for armor item. '.$item->name->en.' - '.$item->id);
//				}
//			}
//		}
//
//		return $this->armorItemCounterparts;
//	}
//
//	public function getFashionItemCounterparts() {
//		if ($this->fashionItemCounterparts == null) {
//			$filteredItems = array_filter($this->getItemData(), function ($item) {
//				return $item->category == "fashion" && $item->sex;
//			});
//			foreach ($filteredItems as $item) {
//				$counterparts = $this->findFashionArmorCounterparts($filteredItems, $item);
//				if (count($counterparts) == 1) {
//					$this->fashionItemCounterparts[$item->id] = $counterparts[0];
//				} else if (count($counterparts) > 1) {
//					$counterpartNames = array_map(function ($cp) {
//						return NamingUtility::getUniqueName($cp->name->en, $cp->id, NamingUtility::IDENTIFIER_ITEM);
//					}, $counterparts);
//					$counterpartNames = array_unique($counterpartNames);
//					if (count($counterpartNames) == 1) {
//						$this->fashionItemCounterparts[$item->id] = $counterparts[0];
//						continue;
//					}
//					echo 'multiple counterparts for fashion item. '.$item->name->en.' - '.$item->id."\n";
//				}
//			}
//		}
//
//		return $this->fashionItemCounterparts;
//	}

//	protected function findFashionArmorCounterparts($itemsToCheck, $item) {
//		$counterparts = [];
//		foreach ($itemsToCheck as $counterpart) {
//			if ($counterpart->id != $item->id
//				&& $counterpart->level == $item->level
//				&& $counterpart->subcategory == $item->subcategory
//				&& $counterpart->sex != $item->sex
//				&& $this->removeSexInFashionItemName($counterpart->name->en) == $this->removeSexInFashionItemName($item->name->en)
//			) {
//				$counterparts[] = $counterpart;
//			}
//		}
//
//		return $counterparts;
//	}

	protected function removeSexInFashionItemName($name) {
		return trim(str_replace(['(M)', '(F)'], '', $name));
	}

//	protected function findArmorItemCounterparts($itemsToCheck, $item) {
//		$counterparts = [];
//		foreach ($itemsToCheck as $counterpart) {
//			if ($counterpart->id != $item->id
//				&& $counterpart->class == $item->class
//				&& $counterpart->level == $item->level
//				&& $counterpart->rarity == $item->rarity
//				&& $counterpart->category == $item->category
//				&& $counterpart->subcategory == $item->subcategory
//				&& $counterpart->sex != $item->sex
//			) {
//				$counterparts[] = $counterpart;
//			}
//		}
//
//		return $counterparts;
//	}

	public function getItemIdsDroppedByMonsters() {
		if ($this->itemIdsDroppedByMonsters == null) {
			$monsters = $this->getApiJsonData('monster');
			foreach ($monsters as $monster) {
				if ($monster->drops) {
					foreach ($monster->drops as $drop) {
						$this->itemIdsDroppedByMonsters[$drop->item]++;
					}
				}
			}
		}

		return $this->itemIdsDroppedByMonsters;
	}
	public function getItemIdsSoldByNPCs() {
		if ($this->itemIdsSoldByNPCs == null) {
			$npcs = $this->getApiJsonData('npc');
			foreach ($npcs as $npc) {
				if ($npc->shop) {
					foreach ($npc->shop as $shop) {
						foreach ($shop->items as $item) {
							$this->itemIdsSoldByNPCs[$item]++;
						}
					}
				}
			}
		}

		return $this->itemIdsSoldByNPCs;
	}
	public function getVersion() {
		return current($this->getApiJsonData('version'));
	}
	public function getItemData() {
		return $this->getApiJsonData('item');
	}
	public function getEquipmentSetData() {
		return $this->getApiJsonData('equipset');
	}
	public function getMonsterData() {
		return $this->getApiJsonData('monster');
	}
	public function getClassData() {
		return $this->getApiJsonData('class');
	}
	public function getNPCData() {
		return $this->getApiJsonData('npc');
	}
	public function getWorldData() {
		return $this->getApiJsonData('world');
	}
	public function getSkillData() {
		return $this->getApiJsonData('skill');
	}
	public function getPartySkillData() {
		return $this->getApiJsonData('partyskill');
	}
	public function getAchievementData() {
		return $this->getApiJsonData('achievement');
	}
	public function getQuestData() {
		return $this->getApiJsonData('quest');
	}

	public function getContinentData() {
		if ($this->continentData == null) {
			foreach ($this->getWorldData() as $d) {
				if ($d->continents) {
					foreach ($d->continents as $continent) {
						$continent->world = DataBag::getBag()->getWorldIdsToNames()[$d->id];
						$this->continentData[] = $continent;
					}
				}
			}
		}

		return $this->continentData;
	}

	public function getItemIdsToNames() {
		if ($this->itemIdsToNames == null) {
			foreach ($this->getItemData() as $d) {
				$this->itemIdsToNames[$d->id] = NamingUtility::getUniqueName($d->name->en, $d->id, NamingUtility::IDENTIFIER_ITEM);
			}
		}

		return $this->itemIdsToNames;
	}

	public function getEquipmentSetIdsToNames() {
		if ($this->equipmentSetIdsToNames == null) {
			foreach ($this->getEquipmentSetData() as $d) {
				$this->equipmentSetIdsToNames[$d->id] = NamingUtility::getUniqueName($d->name->en, $d->id, NamingUtility::IDENTIFIER_EQUIPMENT_SET);
			}
		}

		return $this->equipmentSetIdsToNames;
	}#

	public function getSkillIdsToNames() {
		if ($this->skillIdsToNames == null) {
			foreach ($this->getSkillData() as $d) {
				$this->skillIdsToNames[$d->id] = NamingUtility::getUniqueName($d->name->en, $d->id, NamingUtility::IDENTIFIER_SKILL);
			}
		}

		return $this->skillIdsToNames;
	}

	public function getClassIdsToNames() {
		if ($this->classIdsToNames == null) {
			foreach ($this->getClassData() as $d) {
				$this->classIdsToNames[$d->id] = NamingUtility::getUniqueName($d->name->en, $d->id, NamingUtility::IDENTIFIER_CLASS);
			}
		}

		return $this->classIdsToNames;
	}

	public function getMonsterIdsToNames() {
		if ($this->monsterIdsToNames == null) {
			foreach ($this->getMonsterData() as $d) {
				$this->monsterIdsToNames[$d->id] = NamingUtility::getUniqueName($d->name->en, $d->id, NamingUtility::IDENTIFIER_MONSTER);
			}
		}

		return $this->monsterIdsToNames;
	}

	public function getNPCIdsToNames() {
		if ($this->npcIdsToNames == null) {
			foreach ($this->getNPCData() as $d) {
				$this->npcIdsToNames[$d->id] = NamingUtility::getUniqueName($d->name->en, $d->id, NamingUtility::IDENTIFIER_NPC);
			}
		}

		return $this->npcIdsToNames;
	}

	public function getQuestIDsToNames() {
		if ($this->questIdsToNames == null) {
			foreach ($this->getQuestData() as $d) {
				$this->questIdsToNames[$d->id] = NamingUtility::getUniqueName($d->name->en, $d->id, NamingUtility::IDENTIFIER_QUEST);
			}
		}

		return $this->questIdsToNames;
	}



	public function getWorldIdsToNames() {
		if ($this->worldIdsToNames == null) {
			foreach ($this->getWorldData() as $d) {
				$this->worldIdsToNames[$d->id] = NamingUtility::getUniqueName($d->name->en, $d->id, NamingUtility::IDENTIFIER_WORLD);
			}
		}

		return $this->worldIdsToNames;
	}

	public function getContinentIdsToNames() {
		if ($this->continentIdsToNames == null) {
			foreach ($this->getContinentData() as $d) {
				$this->continentIdsToNames[$d->id] = NamingUtility::getUniqueName($d->name->en, $d->id, NamingUtility::IDENTIFIER_CONTINENT);
			}
		}

		return $this->continentIdsToNames;
	}
}

