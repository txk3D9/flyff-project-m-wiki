<?php

class QuestChainUtility {

	static function findQuestIdsForChain($startQuestId) {
		$parentQuestsToChildQuests = QuestChainUtility::getParentQuestsToChildQuests();
		$questChainIds = QuestChainUtility::determeinQuestChainIds($startQuestId, $parentQuestsToChildQuests);

		return $questChainIds;
	}

	static function getMinMaxLevelFromQuestIds($questIds) {
		$minLevel = null;
		$maxLevel = null;
		foreach($questIds as $questId) {
			$quest = DataBag::getBag()->getQuestData()[$questId];
			$minLevelQuest = $quest->minLevel;
			$maxLevelQuest = $quest->maxLevel;
			if($minLevelQuest && ($minLevelQuest < $minLevel || !$minLevel)) {
				$minLevel = $minLevelQuest;
			}
			if($maxLevelQuest && ($maxLevelQuest > $maxLevel || !$maxLevel)) {
				$maxLevel = $maxLevelQuest;
			}
		}
		return [$minLevel, $maxLevel];
	}

	//key = parent quest id
	//value = child quest ids !
	private static function getParentQuestsToChildQuests() {
		$parentQuestsToChildQuests = [];
		foreach(DataBag::getBag()->getQuestData() as $quest) {
			if ($quest->type == "chain" && $quest->beginQuests) {
				foreach($quest->beginQuests as $beginQuest) {
					$parentQuestsToChildQuests[$beginQuest->quest][$quest->id] = $quest->id;
				}
			}
		}

		return $parentQuestsToChildQuests;
	}

	private static function determeinQuestChainIds(int $startQuestId,
												   array $parentQuestsToChildQuests,
												   $questChainIds = []) {
		if(!in_array($startQuestId)) {
			$questChainIds[] = $startQuestId;
		}
		if(array_key_exists($startQuestId, $parentQuestsToChildQuests)) {
			$questChainIds = array_merge($questChainIds, $parentQuestsToChildQuests[$startQuestId]);

			foreach($parentQuestsToChildQuests[$startQuestId] as $childQuestId) {
				$questChainIds = QuestChainUtility::determeinQuestChainIds($childQuestId, $parentQuestsToChildQuests, $questChainIds);
			}
		}

		return array_unique($questChainIds);
	}


}

