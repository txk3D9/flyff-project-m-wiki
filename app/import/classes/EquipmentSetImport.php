<?php

class EquipmentSetImport extends AbstractApiImport {

	protected $specialyHandledProperties = ['bonus'];
	protected $propertiesHandledToList = ['parts'];

	protected function getNamingIdentifier() {
		return NamingUtility::IDENTIFIER_EQUIPMENT_SET;
	}

	function getCategories($item): array {
		$categories = ['Equipment Set'];
		$wearableClass = $this->getClassNameOfObject($this->getItemFromParts($item));
		if ($wearableClass) {
			$categories[] = 'Equipment Set '.$wearableClass;
		}

		return $categories;
	}

	private function getItemFromParts($item) {
		if ($item->parts) {
			return DataBag::getBag()->getItemData()[$item->parts[0]];
		}
	}

	function handleProperty($propertyName, $propertyValue, $item){
		if($propertyName == 'transy'){
			return DataBag::getBag()->getEquipmentSetIdsToNames()[$propertyValue];
		}

		return parent::handleProperty($propertyName, $propertyValue, $item);
	}

	function handlePropertyToList($propertyName, $propertyValue) {
		if ($propertyName == 'parts') {
			$names = [];
			foreach ($propertyValue as $itemId) {
				$name = DataBag::getBag()->getItemIdsToNames()[$itemId];
				if ($name) {
					$names[] = NamingUtility::getUniqueName($name, $itemId, NamingUtility::IDENTIFIER_ITEM);
				}
			}

			return parent::handlePropertyToList($propertyName, $names);
		} else {
			return parent::handlePropertyToList($propertyName, $propertyValue);
		}
	}


	function additionalProperties($item): array {
		$partItem = $this->getItemFromParts($item);

		return [
			'class' => $this->getClassNameOfObject($partItem),
			'sex' => ($partItem) ? NamingUtility::transformApiValueToWikiName($partItem->sex) : null,
			'level' => ($partItem) ? $partItem->level : null,
		];
	}
}
