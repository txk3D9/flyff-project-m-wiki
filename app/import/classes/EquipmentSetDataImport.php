<?php

class EquipmentSetDataImport extends AbstractDataImport {

	protected function getData() {
		return DataBag::getBag()->getEquipmentSetData();
	}

	protected function getNamingIdentifier() {
		return NamingUtility::IDENTIFIER_EQUIPMENT_SET;
	}

	protected function getContent($eqSet) {
		$contents =  [];
		if ($eqSet->bonus) {
			$bonusRows = $this->getDefaultBonusRows($eqSet);
			if ($bonusRows) {
				$contents[] =  "== Bonus ==\n\n".implode("\n", $bonusRows);
			}
			$bonusRowsInherited = $this->getInheritedBonusRows($eqSet);
			if ($bonusRowsInherited) {
				$contents[] =  "== Bonus Inherited ==\n\n".implode("\n", $bonusRowsInherited);
			}
		}

		if($contents) {
			return parent::getContent($eqSet).implode("\n\n", $contents);
		}
	}

	private function getDefaultBonusRows($eqSet) {
		$bonusResult = [];
		foreach ($eqSet->bonus as $bonus) {
			if ($bonus->ability) {
				$bonusResult[] = $this->createBonusRow($bonus, false);
			}
		}

		return $bonusResult;
	}

	private function getInheritedBonusRows($eqSet) {
		$inheritedBonusRowsResult = [];
		$inheritedBonusRows = $this->generateInheritedBonusRowsStructure($eqSet);
		foreach ($inheritedBonusRows as $bonus) {
			if ($bonus->ability) {
				$inheritedBonusRowsResult[] = $this->createBonusRow($bonus, true);
			}
		}

		return $inheritedBonusRowsResult;
	}

	private function generateInheritedBonusRowsStructure($eqSet) {
		$inheritedBonuse = [];
		$groupByEquipped = [2 => [], 3 => [], 4 => []];
		foreach ($eqSet->bonus as $bonus) {
			$groupByEquipped[(int) $bonus->equipped][] = $bonus;
		}
		foreach ($groupByEquipped as $equipedNumber => $equippedGroup) {
			foreach ($equippedGroup as $bonusFromEquippedGroup) {
				$inheritedBonuse[$equipedNumber][$bonusFromEquippedGroup->ability->parameter] = clone $bonusFromEquippedGroup;
			}
			for ($i = 2; $i < $equipedNumber; $i++) {
				foreach ($groupByEquipped[$i] as $bonusToInherit) {
					if (array_key_exists($bonusToInherit->ability->parameter, $inheritedBonuse[$equipedNumber])) {
						$inheritedBonuse[$equipedNumber][$bonusToInherit->ability->parameter]->ability = clone $inheritedBonuse[$equipedNumber][$bonusToInherit->ability->parameter]->ability;
						$inheritedBonuse[$equipedNumber][$bonusToInherit->ability->parameter]->ability->add += $bonusToInherit->ability->add;
					} else {
						$clone = clone $bonusToInherit;
						$clone->equipped = $equipedNumber;
						$inheritedBonuse[$equipedNumber][$bonusToInherit->ability->parameter] = $clone;
					}
				}
			}
		}
		$result = [];
		foreach ($inheritedBonuse as $bonusGroup) {
			foreach ($bonusGroup as $bonus) {
				$result[] = $bonus;
			}
		}

		return $result;
	}

	private function createBonusRow($bonus, $inherited) {
		$ability = $bonus->ability;

		return '{{Ability|'.implode("|", [
				'equipmentSet={{PAGENAME}}',
				'equippedItemCount='.$bonus->equipped,
				'parameter='.NamingUtility::transformApiValueToWikiName($ability->parameter),
				'add='.$ability->add,
				'set='.$ability->set,
				'inherited='.($inherited ? 1 : 0),
				'rate='.($ability->rate ? 1 : 0),
				'attribute='.NamingUtility::transformApiValueToWikiName($ability->attribute),
				'dotValue='.$ability->dotValue,
				'dotMode='.NamingUtility::transformApiValueToWikiName($ability->dotMode),
				'skill=',
				'skillLevel=',
				'skillChanceSkill='.$ability->skill,
				'skillChanceLevel='.$ability->skillLevel,
				'pvp='.$ability->pve,
				'pve='.$ability->pvp,
			]).'}}';
	}
}