<?php

class HelperUtility {

	static function getImportFolderPath() {
		return dirname(__FILE__).'/../';
	}

	static function getTempFolderPath() {
		return HelperUtility::getImportFolderPath().'temp/';
	}

	static function getWrappedCategoriesArray($categories) {
		//@todo do we want categories?!
		return [];
		$result = [];
		foreach ($categories as $categoryName) {
			$result[] = '[[Category: '.$categoryName.']]';
		}

		return $result;
	}
}