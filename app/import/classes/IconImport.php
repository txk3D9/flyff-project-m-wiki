<?php

class IconImport extends AbstractImport {

	protected $identifier;

	function __construct($identifier) {
		$this->identifier = $identifier;
	}

	function execute() {
		$iconDir = HelperUtility::getImportFolderPath().'api_icons/'.$this->identifier.'/';
		if(file_exists($iconDir)) {
			if($this->debugOutput) {
				echo "start import ".$this->identifier." icons\n";
			}
			MediaWikiUtility::importImageFromFolder($iconDir);
			if($this->debugOutput) {
				echo "end import ".$this->identifier." icons\n";
			}
			if($this->debugOutput) {
				echo "start set categories for ".$this->identifier." icons\n";
			}
			$icons = array_diff(scandir($iconDir), array('.', '..'));
			$this->setCategoryToIcon($icons);
			if($this->debugOutput) {
				echo "end set categories for ".$this->identifier." icons\n";
			}
		} else {
			throw new \Exception("Icon dir for ".$this->identifier." does not exist: ".$iconDir);
		}
	}

	function setCategoryToIcon($icons) {
		$categories = $this->getCategories();
		if($categories) {
			$tempFile = HelperUtility::getImportFolderPath().'temp/icon_categories_.html';
			foreach($icons as $iconFileName) {
				file_put_contents($tempFile, implode(" ", $categories));
				MediaWikiUtility::saveFileToPage("File:".$iconFileName, $tempFile);
			}

			if($this->keepTempFiles) {
				unlink($tempFile);
			}
		}
	}

	protected function getCategories() {
		return HelperUtility::getWrappedCategoriesArray(
			['Icon', 'Icon '.NamingUtility::identifierToUppercase($this->identifier)]
		);
	}
}