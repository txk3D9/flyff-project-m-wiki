<?php

abstract class AbstractApiImport extends AbstractImport {

	protected $specialyHandledProperties = []; //have to be handeld in seperat import
	protected $propertiesHandledToList = []; //array will be formated to list
	protected $localizationPropertiesHandledToString = ["name", "description"]; //name, description
	protected $unhandledProperties = []; //skip these completley
	protected $passthroughProperties = []; //will be passthroughed completely. probably handeld in "handleProperty()" function
	protected $apiValueToWikiNameProperties = [];

	protected function getNamingIdentifier() {
		return ucfirst($this->getIdentifier());
	}

	function execute() {
		$data = $this->getApiJsonData();
		foreach ($data as $item) {
			$itemName = NamingUtility::getUniqueName($item->name->en, $item->id, $this->getNamingIdentifier());
			if ($this->debugOutput) {
				echo "Import ".$this->getIdentifier().": ".$itemName;
			}
			if (!$itemName) {
				echo " - FAIL. item has no name. item id: ".$item->id." \n";
				continue;
			}
			$itemResults = $this->getItemResult($item);
			$content = $this->getFileContent($item, $itemResults);
			$this->saveContentToPage($content, $itemName);
			if ($this->debugOutput) {
				echo " - done \n";
			}
		}
	}

	protected function saveContentToPage($content, $itemName) {
		$fileDir = HelperUtility::getImportFolderPath().'/temp/'.$this->getIdentifier().'/';
		if (!file_exists($fileDir)) {
			mkdir($fileDir);
		}
		$fileNameEscaped = MediaWikiUtility::escapePageName($itemName);
		$file = $fileDir.$fileNameEscaped.'.html';
		file_put_contents($file, $content);
		MediaWikiUtility::saveFileToPage($fileNameEscaped, $file);
		if (!$this->keepTempFiles) {
			unlink($file);
		}
	}

	protected function getFileContent($item, $itemResult) {
		[$search, $replace] = $this->getExtraTemplateReplacements($item);
		$categories = HelperUtility::getWrappedCategoriesArray($this->getCategories($item));
		$searchArray = array_merge(['$data', '$categories'], $search);
		$repplaceArray = array_merge(["\t|".implode("\n\t|", $itemResult), implode(" ", $categories),], $replace);
		$templateFilePath =  HelperUtility::getImportFolderPath().'/templates/data/'.$this->getIdentifier().'.html';
		$pageFilePath = ($item->name->en) ? HelperUtility::getImportFolderPath().'/pages/'.$item->name->en.'.html' : null;
		if($pageFilePath && file_exists($pageFilePath)) {
			$fileContent = file_get_contents($pageFilePath);
		} else {
			$fileContent = file_get_contents($templateFilePath);
		}
		return str_replace($searchArray, $repplaceArray, $fileContent);
	}

	protected function getItemResult($item) {
		$itemResult = [];
		foreach ($item as $propertyName => $propertyValue) {
			if (in_array($propertyName, $this->propertiesHandledToList)) {
				$propertyValue = $this->handlePropertyToList($propertyName, $propertyValue);
			} else if (is_bool($propertyValue)) {
				$propertyValue = ($propertyValue) ? 1 : 0;
			} else if (
				!in_array($propertyName, $this->passthroughProperties)
				&& !in_array($propertyName, $this->localizationPropertiesHandledToString)
				&& (
					is_object($propertyValue)
					|| in_array($propertyName, $this->specialyHandledProperties)
					|| in_array($propertyName, $this->unhandledProperties))
			) {
				continue;
			}
			$propertyValue = $this->handleProperty($propertyName, $propertyValue, $item);
			$itemResult[$propertyName] = $propertyName."=".$this->transformApiValueToWikiName($propertyName, $propertyValue);
		}
		foreach ($this->additionalProperties($item) as $propertyName => $propertyValue) {
			$itemResult[$propertyName] = $propertyName."=".$this->transformApiValueToWikiName($propertyName, $propertyValue);;
		}

		return $itemResult;
	}

	protected function additionalProperties($item): array {
		return [];
	}

	function handlePropertyToList($propertyName, $propertyValue) {
		return implode(", ", $propertyValue);
	}

	function handleProperty($propertyName, $propertyValue, $item) {
		if (in_array($propertyName, $this->localizationPropertiesHandledToString)) {
			$propertyValue = $propertyValue->en;
		}

		if($propertyName == 'description') {
			$propertyValue = TextReplacementUtility::replaceTextWithLinkMarkers($propertyValue);
		}

		return $propertyValue;
	}

	abstract function getCategories($item): array;

	function getExtraTemplateReplacements($item) {
		return [[], []];
	}

	function getIdentifier() {
		return str_replace("import", "", strtolower(get_class($this)));
	}

	function getApiJsonData(): array {
		$data = DataBag::getBag()->getApiJsonData($this->getIdentifier());
		if ($this->cliArguments[2]) {
			$cliArguments = $this->cliArguments;
			unset($cliArguments[0], $cliArguments[1]);
			foreach ($data as $key => $d) {
				if (!in_array($d->id, $cliArguments)) {
					unset($data[$key]);
				}
			}
		}

		return $data;
	}

	protected function getClassNameOfObject($item) {
		if ($item) {
			if ($item->class) {
				return DataBag::getBag()->getClassIdsToNames()[$item->class];
			}
		}
	}

	protected function transformApiValueToWikiName($propertyName, $propertyValue) {
		if (in_array($propertyName, $this->apiValueToWikiNameProperties)) {
			return NamingUtility::transformApiValueToWikiName($propertyValue);
		} else {
			return $propertyValue;
		}
	}
}