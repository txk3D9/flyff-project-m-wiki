<?php

class QuestDataImport extends AbstractDataImport {

	use SpawnImport;

	protected function getData() {
		return DataBag::getBag()->getQuestData();
	}

	protected function getNamingIdentifier() {
		return NamingUtility::IDENTIFIER_QUEST;
	}

	protected function getContent($quest) {
		$contents = [];
		$chainQuest = $this->getChainQuest($quest);
		if ($chainQuest) {
			$contents[] = "== Chain Quest ==\n\n".$chainQuest;
		}
		$itemRewards = $this->getItemRewards($quest);
		if ($itemRewards) {
			$contents[] = "== Item Rewards ==\n\n".implode("\n", $itemRewards);
		}
		$todos = $this->getTodos($quest);
		if ($todos) {
			$contents[] = "== Todos ==\n\n".implode("\n", $todos);
		}
		$locationRows = $this->getSpawnRows($quest->endVisitPlace);
		if ($locationRows) {
			$contents[] = "== End Visit Place ==\n\n".implode("\n", $locationRows);
		}
		if ($contents) {
			return parent::getContent($quest).implode("\n\n", $contents);
		}
	}

	protected function getItemRewards($quest) {
		$rows = [];
		if ($quest->endReceiveItems) {
			foreach ($quest->endReceiveItems as $item) {
				$rows[] = '{{Quest Item Reward|'.implode("|", [
						'item='.DataBag::getBag()->getItemIdsToNames()[$item->item],
						'count='.$item->count,
						'soulLinked='.($item->soulLinked ? 1 : 0),
					]).'}}';
			}
		}

		return $rows;
	}

	protected function getTodos($quest) {
		$rows = [];
		if ($quest->endNeededItems) {
			foreach ($quest->endNeededItems as $item) {
				$rows[] = '{{Quest Todo|'.implode("|", [
						'item='.DataBag::getBag()->getItemIdsToNames()[$item->item],
						'count='.$item->count,
					]).'}}';
			}
		}
		if ($quest->endKillMonsters) {
			foreach ($quest->endKillMonsters as $item) {
				$rows[] = '{{Quest Todo|'.implode("|", [
						'monster='.DataBag::getBag()->getMonsterIdsToNames()[$item->monster],
						'count='.$item->count,
					]).'}}';
			}
		}

		return $rows;
	}

	protected function getChainQuest($quest) {
		$questChain = null;
		if ($quest->type == "chain" && !$quest->beginQuests) { //is first quest of chain
			$questIds = QuestChainUtility::findQuestIdsForChain($quest->id);
			if ($questIds) {
				$quests = [];
				$parentId = DataBag::getBag()->getQuestData()[current($questIds)]->parent;
				$parent = NamingUtility::getUniqueName(DataBag::getBag()->getQuestData()[$parentId]->name->en, $parentId, NamingUtility::IDENTIFIER_QUEST);
				foreach ($questIds as $questId) {
					$quests[] = DataBag::getBag()->getQuestIDsToNames()[$questId];
				}
				if ($quests) {
					[$minLevel, $maxLevel] = QuestChainUtility::getMinMaxLevelFromQuestIds($questIds);
					$questChain = '{{Quest Chain|'.implode("|", [
							'quests='.implode("{{!}}", $quests),
							'parent='.$parent,
							'minLevel='.$minLevel,
							'maxLevel='.$maxLevel
						]).'}}'."\n\n";
				}
			}
		}

		return $questChain;
	}
}