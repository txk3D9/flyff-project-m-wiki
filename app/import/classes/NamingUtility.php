<?php

class NamingUtility {

	private static $duplicateNames = null;
	const IDENTIFIER_SKILL = 'Skill';
	const IDENTIFIER_MONSTER = 'Monster';
	const IDENTIFIER_CLASS = 'Class';
	const IDENTIFIER_ABILITY = 'Ability';
	const IDENTIFIER_NPC = 'NPC';
	const IDENTIFIER_ITEM = 'Item';
	const IDENTIFIER_WORLD = 'World';
	const IDENTIFIER_PARTY_SKILL = 'Party skill';
	const IDENTIFIER_QUEST = 'Quest';
	const IDENTIFIER_ACHIEVEMENT = 'Achievement';
	const IDENTIFIER_CONTINENT = 'Continent';
	const IDENTIFIER_EQUIPMENT_SET = 'Equipment set';
	const UNIQUE_NAME_IDENTIFIER_PRIORITY = [
		self::IDENTIFIER_MONSTER,
		self::IDENTIFIER_CLASS,
		self::IDENTIFIER_ITEM,
		self::IDENTIFIER_SKILL,
		self::IDENTIFIER_PARTY_SKILL,
		self::IDENTIFIER_EQUIPMENT_SET,
		self::IDENTIFIER_WORLD,
		self::IDENTIFIER_NPC,
		self::IDENTIFIER_QUEST,
		self::IDENTIFIER_ACHIEVEMENT,
		self::IDENTIFIER_CONTINENT,
		self::IDENTIFIER_ABILITY
	];
	const IDENTIFIER_TO_DATA_SOURCE = [
		self::IDENTIFIER_MONSTER => 'monster',
		self::IDENTIFIER_CLASS => 'class',
		self::IDENTIFIER_WORLD => 'world',
		self::IDENTIFIER_PARTY_SKILL => 'partyskill',
		self::IDENTIFIER_ITEM => 'item',
		self::IDENTIFIER_QUEST => 'quest',
		self::IDENTIFIER_ACHIEVEMENT => 'achievement',
		self::IDENTIFIER_SKILL => 'skill',
		self::IDENTIFIER_EQUIPMENT_SET => 'equipmentset',
		self::IDENTIFIER_NPC => 'npc',
		self::IDENTIFIER_CONTINENT => 'continent',
		self::IDENTIFIER_ABILITY => 'abilityParameters'
	];
	const API_VALUE_TO_WIKI_NAME = [
		'none' => 'none',
		//item category
		//		"booty" => "Quest item",
		"Quest item" => "Quest item",
		"pickuppet" => "Pickup pet",
		"monsterball" => "Monster ball",
		"giftbox" => "Gift box",
		"vendorskin" => "Vendor skin",
		"teleportring" => "Teleport ring",
		//item subcategory
		"upgradedice" => "Upgrade dice",
		"powerdice" => "Power dice",
		"elementcard" => "Element card",
		"piercingcard" => "Piercing card",
		"piercingdice" => "Piercing dice",
		"protectscroll" => "Protect scroll",
		"randomscroll" => "Random scroll",
		"specialstone" => "Special stone",
		"armorcolor" => "Armor color",
		"petfeed" => "Pet feed",
		"balloon" => "Balloon",
		"inventorybag" => "Inventory bag",
		"townblinkwing" => "Town blinkwing",
		"ultimatedice" => "Ultimate dice",
		"selectblinkwing" => "Select Blinkwing",
		"awakescroll" => "Awake scroll",
		"visualcloak" => "Visual cloak",
		"globalgacha" => "Global gacha",
		"gacha" => "Gacha",
		"giftbox" => "Gift box",
		"ampexp" => "AMP exp",
		"upgradescroll" => "Upgrade scroll",
		//item rarity
		"veryrare" => "Very rare",
		//atk speed
		"veryslow" => "Very slow",
		"veryfast" => "Very fast",
		//ability
		"attackspeed" => "Attack speed",
		"attackspeedrate" => "Attack speed rate",
		"jumpheight" => "Jump height",
		"bowrange" => "Bow range",
		"reflectdamage" => "Reflect damage",
		"rangedblock" => "Ranged block",
		"meleeblock" => "Melee block",
		"magicdefense" => "Magic defense",
		"electricitydefense" => "Electricity defense",
		"firedefense" => "Fire defense",
		"winddefense" => "Wind defense",
		"waterdefense" => "Water defense",
		"earthdefense" => "Earth defense",
		"yoyoorbow" => "Yoyo or bow",
		"wandorstaff" => "Wand or staff",
		"enchantedweapon" => "Enchanted weapon",
		"maxhp" => "Max HP",
		"maxmp" => "Max MP",
		"maxfp" => "Max FP",
		"magicattack" => "Magic attack",
		"swordattack" => "Sword attack",
		"axeattack" => "Axe attack",
		"knuckleattack" => "Knuckle attack",
		"yoyoattack" => "Yoyo attack",
		"bowattack" => "Bow attack",
		"earthmastery" => "Earth mastery",
		"firemastery" => "Fire mastery",
		"watermastery" => "Water mastery",
		"electricitymastery" => "Electricity mastery",
		"windmastery" => "Wind mastery",
		//skill target
		"currentplayer" => "Current player",
		"hitrate" => "Hit rate",
		"criticalchance" => "Critical chance",
		"pvpdamagereduction" => "PVP damage reduction",
		"elementattack" => "Element attack",
		"skillchance" => "Skill chance",
		"hprecovery" => "HP recovery",
		"mprecovery" => "MP recovery",
		"fprecovery" => "FP recovery",
		"fprecoveryautoattack" => "FP recovery auto attack",
		"hprecoveryafterkill" => "HP recovery after kill",
		"mprecoveryafterkill" => "MP recovery after kill",
		"fprecoveryafterkill" => "FP recovery after kill",
		"decreasedmpconsumption" => "Decreased MP consumption",
		"decreasedfpconsumption" => "Decreased FP consumption",
		"minability" => "Min ability",
		"maxability" => "Max ability",
		"attributeimmunity" => "Attribute immunity",
		"autohp" => "Auto HP",
		"decreasedcastingtime" => "Decreased casting time",
		"criticaldamage" => "Critical damage",
		"skilldamage" => "Skill damage",
		"hprestoration" => "HP restoration",
		"criticalresist" => "Critical resist",
		"pvpdamagereduction" => "PVP damage reduction",
		"magicdefense" => "Magic defense",
		"pvpdamage" => "PVP damage",
		"pvedamage" => "PVE damage",
		"allelementsdefense" => "All elements defense",
		"allstats" => "All stats",
		"attackandmaxhp" => "Attack and max HP",
		"defenseandhitratedecrease" => "Defense and hit rate decrease",
		"allelementsmastery" => "All elements mastery",
		"allrecovery" => "All recovery",
		"allrecoveryafterkill" => "All recovery after kill",
		"decreasedfpandmpconsumption" => "Decreased FP and MP consumption",
		"removealldebuff" => "Remove all debuff",
		"removedebuff" => "Remove debuff",
		"damageandstealhp" => "Damage and steal HP",
		"stealhp" => "Steal HP",
		"explostdecreaseatrevival" => "EXP lost decreased revival",
		//skill target
		//atributes
		"hitrateandpoison" => "Hit rate and poison",
		"hitrateandpoisonandstun" => "Hit rate and poison and stun",
		"counterattackdamage" => "Counter attack damage",
		"lootandslow" => "Loot and slow",
		"poisonandbleeding" => "Poison and bleeding",
		"stunandrooting" => "Stun and rooting",
		"moonbeam" => "Moon Beam",
		"poisonandbleedingandmoonbeam" => "Poison and bleeding Moonbeam",
		//dot mode
		"currentdamage" => "Current damage",
		"standardattack" => "Attack",
		"fixedvalue" => "Fixed",
		//world places
		"flyingstation" => "Flying staiton",
		//
		"weaponstore" => "Weapon store",
		"armorstore" => "Armor store",
		"foodstore" => "Food store",
		"magicstore" => "Magic store",
		"generalstore" => "General store",
		"publicoffice" => "Public office",
		"questoffice" => "Quest office",
		"shieldstore" => "Shield store",
		"warpzone" => "Warp zone",
		//achievements
		"killmonster" => "Kill monster",
		"useitem" => "Use item",
		"useskill" => "Use skill",
		"incomingdamage" => "Incoming damage",
		"spiritstrike" => "Spirit strike",
		"stealfp" => "Steal FP"
	];
	const API_VALUE_TO_WIKI_NAME_UCFIRST = [
		//elements
		'fire', 'water', 'electricity', 'earth', 'wind',
		//class,
		"beginner", "expert", "professional",
		//world
		"main", "prison", "dungeon", "instance", "event",
		//monster rank
		"small", "normal", "captain", "giant", "boss", "material", "super", "guard", "violet",
		//monster experienceSharing
		"normal", "area",
		//item category
		"weapon", "armor", "fashion", "jewelry", "flying", "collector",
		"quest", "trans", "fuel", "arrow", "charm", "recovery", "blinkwing",
		"firework", "material", "buff", "pack", "scroll",
		"gem",
		"piece",
		"selectbox",
		"hoverbike",
		//item subcategory
		"stick", "yoyo", "knuckle", "wand", "sword", "axe", "shield",
		"staff", "bow", "suit", "helmet", "gauntlet", "boots", "ring", "mineral", "shoes", "food", "mask",
		"cloak", "cloth", "glove", "hat", "pill", "booster", "necklace", "broom", "board", "wings", "car",
		"earring", "key", "glow", "drink", "refresher",
		"booty",
		"book",
		"letter",
		//item rarity
		"common", "uncommon", "rare", "unique",
		//sex (yes please)
		"male", "female",
		//atk speed
		"slow", "normal", "fast", "fastest",
		//ability
		"speed", "def", "parry", "attack",
		//ability
		"damage",
		"attribute",
		"healing",
		"penya",
		"block",
		"cure", "movement",
		"cheerpoint",
		"party", "line", "area", "single",
		//skill combo
		"general", "step", "circle", "finish",
		//attributes
		"groggy", "stun", "invisibility", "poison", "slow", "double",
		"bleeding", "silent",
		"loot", "counterattack",
		//parameter
		"duration",
		//world places
		"lodestar", "lodelight", "dungeon",
		//misc
		//achievements
		"jump",
		"stat",
		"class",
		"level",
		"playtime",
		"connection",
		"general",
		"monsters",
		"consumables",
		"attendance",
		//quest
		"category",
		"normal",
		"repeat",
		"chain",
		"exprate",
		"droprate"
	];
	const API_VALUE_TO_WIKI_NAME_UPPERCASE = [
		"str", "dex", "int", "sta", "npc", "hp", "mp", "fp",
	];

	static function transformApiValueToWikiName($value = '') {
		if ($value) {
			if (array_key_exists($value, self::API_VALUE_TO_WIKI_NAME)) {
				return self::API_VALUE_TO_WIKI_NAME[$value];
			} else if (in_array($value, self::API_VALUE_TO_WIKI_NAME_UPPERCASE)) {
				return strtoupper($value);
			} else if (in_array($value, self::API_VALUE_TO_WIKI_NAME_UCFIRST)) {
				return ucfirst($value);
			} else {
				//'Quest item' is hardcoded for items that have "quest" as category
				throw new \Exception($value.' has no name transformation yet!');
			}
		}

		return $value;
	}

	static function getUniqueName($name, $id = null, $identifier = null) {
		if (!$id && !$identifier) {
			//abilities dont have an ID
			throw new \Exception('either ID or identifier has to be specified');
		}
		$transformedName = trim(strtolower($name));
		$duplicateNames = self::getDuplicateNames();
		if (array_key_exists($transformedName, $duplicateNames) && $duplicateNames[$transformedName]['overall'] > 1) {
			//run through the unique name identifier property
			foreach (self::UNIQUE_NAME_IDENTIFIER_PRIORITY as $priorityIdentifier) {
				if (array_key_exists($priorityIdentifier, $duplicateNames[$transformedName])) {
					//and the propertyIdentifier is the first one in the array (so it has highest priority)
					//and there is only 1 match for this propertyIdentifier -> take the normal name and dont postfix it
					if ($duplicateNames[$transformedName][$priorityIdentifier] == 1
						&& array_keys($duplicateNames[$transformedName])[1] == $identifier) {
						return MediaWikiUtility::escapePageNameKeepWhitespaces($name);
					} else { //otherwise, this name is not prioritzed -> add postfix
						break;
					}
				}
			}
			//if the name is only once per identifier
			if ($identifier && self::getDuplicateNames()[$transformedName][$identifier] == 1) {
				$name = $name.' ('.$identifier.')';
			} else if ($identifier && $id) {
				$name = $name.' ('.$identifier.':'.$id.')';
			} else if ($id) {
				$name = $name.' ('.$id.')';
			} else if ($identifier) {
				$name = $name.' ('.$identifier.')';
			}
		}

		return MediaWikiUtility::escapePageNameKeepWhitespaces($name);
	}

	private static function getDuplicateNames() {
		if (self::$duplicateNames == null) {
			foreach (self::UNIQUE_NAME_IDENTIFIER_PRIORITY as $identifier) {
				$data = DataBag::getBag()->getApiJsonData(self::IDENTIFIER_TO_DATA_SOURCE[$identifier]);
				foreach ($data as $d) {
					if ($identifier == self::IDENTIFIER_ABILITY) {
						$name = NamingUtility::transformApiValueToWikiName($d);
					} else {
						$name = $d->name->en;
					}
					$name = trim(strtolower($name));
					if ($name) {
						self::$duplicateNames[$name]['overall']++;
						self::$duplicateNames[$name][$identifier]++;
					}
				}
			}
		}

		return self::$duplicateNames;
	}

	static function identifierToUppercase($identifier) {
		if ($identifier == 'npc') {
			return strtoupper($identifier);
		} else {
			return ucfirst($identifier);
		}
	}

	static function getTableNameFromSchemaName($name) {
		if ($name != 'Karma') {
			$lastChar = substr($name, -1);
			if ($lastChar == 's') {
				$name = $name.'es';
			} else if ($lastChar == 'y') {
				$name = substr($name, 0, strlen($name) - 1).'ies';
			} else {
				$name = $name.'s';
			}
		}

		return strtolower($name);
	}

	static function getDBFriendlyPropertyName($propertyName) {
		$a = [
			'int' => 'intelligence',
			'add' => 'valueIsAdded',
			'set' => 'valueIsSet',
			'left' => 'posLeft',
			'right' => 'posRight',
		];

		return (array_key_exists($propertyName, $a)) ? $a[$propertyName] : $propertyName;
	}
}