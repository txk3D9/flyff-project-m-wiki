<?php

class ContinentDataImport extends AbstractDataImport {

	protected function getData() {
		return DataBag::getBag()->getContinentData();
	}

	protected function getNamingIdentifier() {
		return NamingUtility::IDENTIFIER_CONTINENT;
	}

	protected function getContent($continent) {
		$contents = [];
		$polygonRows = $this->getPolygonRows($continent);
		if ($polygonRows) {
			$contents[] = "== Polygons ==\n\n".implode("\n", $polygonRows);
		}
		if ($contents) {
			return parent::getContent($continent).implode("\n\n", $contents);
		}
	}

	private function getPolygonRows($continent) {
		$rows = [];
		if ($continent->polygon) {
			foreach ($continent->polygon as $polygon) {
				$rows[] =  '{{Polygon|'.implode("|", [
						'continent={{PAGENAME}}',
						'x='.$polygon->x,
						'Z='.($polygon->Z ? $polygon->Z : $polygon->z),
					]).'}}';
			}
		}

		return $rows;
	}

}