<?php

abstract class AbstractDataImport extends AbstractImport {

	function execute() {
		if ($this->debugOutput) {
			echo "this will take some time\n";
		}
		$data = $this->getData();
		foreach ($data as $d) {
			if ($d->id == $this->cliArguments[2]) {
				$data = [$d];
				break;
			}
		}
		foreach ($data as $item) {
			$pageName = NamingUtility::getUniqueName($item->name->en, $item->id, $this->getNamingIdentifier());
			if($this->debugOutput) {
				echo "Importing ".$pageName;
			}
			$content = $this->getContent($item);
			if($content) {
				$this->savePage("Data:".$pageName, $content);
			}
			if($this->debugOutput) {
				echo " - done\n";
			}
		}
	}

	abstract protected function getData();
	abstract protected function getNamingIdentifier();

	protected function getContent($item) {
		return "Data automatically generated at ".date('Y-m-d H:i:s')." 
		(API Version ".DataBag::getBag()->getVersion().", Import Version ".DataBag::IMPORT_VERSION.")\n";
	}
}