<?php

class RedirectImport extends AbstractImport {

	function execute() {
		$redirects = [
			//general
			'Main_Page' => "Home",
			"Class" => "Classes",
			"Item" => "Items",
			"Element" => "Elements",
			"Consumable" => "Consumable items",
			"Premium" => "Premium items",
			//weapons
			"Weapon" => "Weapons",
			"Yoyo" => "Yoyos",
			"Bow" => "Bows",
			"Stick" => "Sticks",
			"Staff" => "Staffs",
			"Knuckle" => "Knuckles",
			"Sword" => "Swords",
			"Axe" => "Axes",
			"Wand" => "Wands",
			"Shield" => "Shields",
			//armor
			"Helmet" => "Armor",
			"Suit" => "Armor",
			"Gauntlet" => "Armor",
			"Boot" => "Armor",
			"Helmets" => "Armor",
			"Suits" => "Armor",
			"Gauntlets" => "Armor",
			"Boots" => "Armor",
			//jewelry
			"Earring" => "Jewelry#Earrings",
			"Earrings" => "Jewelry#Earrings",
			"Ring" => "Jewelry#Rings",
			"Rings" => "Jewelry#Rings",
			"Necklace" => "Jewelry#Earrings",
			"Necklaces" => "Jewelry#Necklaces",
			//flying
			"Board" => "Flying#Boards",
			"Broom" => "Flying#Brooms",
			"Wings" => "Flying#Wings",
			//stats
//			"STA" => "Stats#STA",
//			"STR" => "Stats#STR",
//			"DEX" => "Stats#DEX",
//			"INT" => "Stats#INT",
			'Health' => 'HP',
			'Health point' => 'HP',
			'Health points' => 'HP',
			'Mana' => 'MP',
			'Mana point' => 'MP',
			'Mana points' => 'MP',
			'Fighting point' => 'FP',
			'Fighting points' => 'FP',
			//recovery
			"Drink" => "Recovery#Drinks",
			"Drinks" => "Recovery#Drinks",
			"Food" => "Recovery#Foods",
			"Foods" => "Recovery#Foods",
			"Pill" => "Recovery#Pills",
			"Pills" => "Recovery#Pills",
			"Refresher" => "Recovery#Refresher",
			"Refreshers" => "Recovery#Refreshers",
			//monsters/ranks
			'Monster' => 'Monsters',
			'Giant' => 'Giant Monsters',
			'Giants' => 'Giant Monsters',
			'Super' => 'Super Monsters',
			'Supers' => 'Super Monsters',
			'Boss' => 'Boss Monsters',
			'Bosses' => 'Boss Monsters',
			'Small Monster' => 'Standard Monsters',
			'Normal Monster' => 'Standard Monsters',
			'Captain Monster' => 'Standard Monsters',
			'Small' => 'Standard Monsters',
			'Normal' => 'Standard Monsters',
			'Captain' => 'Standard Monsters',
			//material / cards / dices
			"Material" => "Item upgrading",
			"Materials" => "Item upgrading",
			"Element card" => "Item upgrading#Element cards",
			"Element cards" => "Item upgrading#Element cards",
			"Piercing card" => "Item upgrading#Piercing cards",
			"Piercing cards" => "Item upgrading#Piercing cards",
			"Piercing dice" =>"Item upgrading#Piercing dices",
			"Piercing dices" =>"Item upgrading#Piercing dices",
			"Upgrade dice" => "Item upgrading#Upgrade dices",
			"Upgrade dices" => "Item upgrading#Upgrade dices",
			"Protect scroll" => "Item upgrading#Protect scrolls",
			"Protect scrolls" => "Item upgrading#Protect scrolls",
			//ability
			"Def" => "Defense",
			//other
			'Quest item' => 'Quest items',
			'Consumable' => 'Consumables',
			'Premium' => 'Premium items',
			'Speed' => 'Movement speed',
			"Booty" => 'Quests#Booty',
			'Buff' => 'Buffs',
			'Key' => 'Keys',
			'Quest' => 'Quests',
			'Teleport ring' => 'Teleport rings',
			'PK' => 'P.K',
			'Gift box' => 'Gift boxes',
			'Pickup pet' => 'Pickup pets',
			'Balloon' => 'Balloons',
			'Mask' => 'Fashion#Masks',
			'Masks' => 'Fashion#Masks',
			'Cloak' => 'Fashion#Cloaks',
			'Cloaks' => 'Fashion#Cloaks',
			'Hat' => 'Fashion#Hats',
			'Hats' => 'Fashion#Hats',
			'Cloth' => 'Fashion#Cloths',
			'Cloths' => 'Fashion#Cloths',
			'Glove' => 'Fashion#Gloves',
			'Gloves' => 'Fashion#Gloves',
			'Shoe' => 'Fashion#Shoes',
			'Shoes' => 'Fashion#Shoes',
			'Scroll' => 'Scrolls',
			'Monster ball' => 'Monster balls',
			'Glow' => 'Glows',
			'Special stone' => 'Special stones',
			'Gold' => 'Penya',
			"Firework" => "Fireworks",
			"Letter" => "Letters",
			"Blinkwing" => "Blinkwings",
			"Random scroll" => "Random scrolls",
			"Arrow" => "Arrows",
			"Charm" => "Charms",
			"Inventory bag" => "Inventory bags",
			"Fuel" => "Fuels",
			"Car" => "Flying#Cars",
			"Vendor skin" => "Vendor skins",
			"Pack" => "Packs",
			"Book" => "Books",
			"Guards" => "Guardians",
			"Town blinkwing" => "Town Blinkwing" //subcategory has only 1 item -> refer to this item
		];

		foreach($redirects as $pageName => $redirectName) {
			$pageName = MediaWikiUtility::escapePageName($pageName);
			$dir = HelperUtility::getTempFolderPath().'redirects/';
			if (!file_exists($dir)) {
				mkdir($dir);
			}
			$filePath = $dir.$pageName.'.html';
			$content = "#REDIRECT [[".$redirectName."]]";
			file_put_contents($filePath, $content);
			MediaWikiUtility::saveFileToPage(
				$pageName,
				$filePath
			);
			if (!$this->keepTempFiles) {
				unlink($filePath);
			}
		}
	}
}