<?php

class RecreateCargoTables extends AbstractImport {

	function execute() {
		$tablesToRecreate = ["drops", "monster_experience", "shops", "attacks", "skill_levels",
							 "scalings", 'achievement_levels', 'achievement_level_items',
							 'quest_chains', 'quest_item_rewards', 'quest_todos'];
		foreach($tablesToRecreate as $tableName) {
			if ($this->debugOutput) {
				echo "Recreate cargo table: ".$tableName;
			}
			MediaWikiUtility::recreateCargoTable($tableName);
			if ($this->debugOutput) {
				echo " - done\n";
			}
		}

	}
}

