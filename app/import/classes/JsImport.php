<?php

class JsImport extends AbstractImport {

	function execute() {
		$jsFilesDir = HelperUtility::getImportFolderPath().'/js';
		$files = array_diff(scandir($jsFilesDir), array('.', '..'));
		foreach ($files as $file) {
			if (!in_array($file, array(".", ".."))) {
				MediaWikiUtility::saveFileToPage("MediaWiki:".ucfirst($file), $jsFilesDir.'/'.$file);
			}
		}
	}
}
