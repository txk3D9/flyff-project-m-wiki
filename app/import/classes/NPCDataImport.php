<?php

class NPCDataImport extends AbstractDataImport {

	use LocationImport;

	protected function getData() {
		return DataBag::getBag()->getNPCData();
	}

	protected function getNamingIdentifier() {
		return NamingUtility::IDENTIFIER_NPC;
	}

	protected function getContent($npc) {
		$contents = [];
		$shopRows = $this->getShopRows($npc);
		if ($shopRows) {
			$contents[] = "== Shops ==\n\n".implode("\n", $shopRows);
		}
		$locationRows = $this->getLocationRows($npc->locations);
		if ($locationRows) {
			$contents[] = "== Location ==\n\n".implode("\n", $locationRows);
		}
		if ($contents) {
			return parent::getContent($npc).implode("\n\n", $contents);
		}
	}

	private function getShopRows($npc) {
		$shopRows = [];
		if ($npc->shop) {
			foreach ($npc->shop as $shop) {
				$items = [];
				foreach ($shop->items as $item) {
					$items[] = DataBag::getBag()->getItemIdsToNames()[$item];
				}
				$shopRows[] = '{{Shop|name='.$shop->name->en.'|items='.implode(",", $items).'}}';
			}
		}

		return $shopRows;
	}
}