<?php

class ClassImport extends AbstractApiImport {

	protected $apiValueToWikiNameProperties = ['type'];

	function getCategories($item): array {
		$categories = ['Class'];

//		$typeToJob = [
//			"beginner" => "1st",
//			"beginner" => "2nd",
//			"beginner" => "3rd",
//		];
//		$categories[] = $typeToJob[$item->type. " Job"];

		return $categories;
	}

	function getExtraTemplateReplacements($class) {
		$parentClass = ($class->parent && $class->minLevel == 60) ? DataBag::getBag()->getClassIdsToNames()[$class->parent] : null;
		return [
			['$classes', '$minLevel', '$maxLevel'],
			[
				'"'.DataBag::getBag()->getClassIdsToNames()[$class->id].'"'.($parentClass ? ',"'.$parentClass.'"' : ''),
				$class->minLevel,
				$class->maxLevel,
			],
		];
	}

	function handleProperty($propertyName, $propertyValue, $item) {
		if($propertyName == 'parent') {
			return DataBag::getBag()->getClassIdsToNames()[$propertyValue];
		} else if(in_array($propertyName, ['maxMP', 'maxFP', 'maxHP'])) {
			return str_replace(["+", "*"], [" + ", " * "], $propertyValue);
		} else {
			return parent::handleProperty($propertyName, $propertyValue, $item);
		}
	}
}