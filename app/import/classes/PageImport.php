<?php

class PageImport extends AbstractImport {

	function execute() {
		//get redirects from specific data and make them into plural?
		$dir = HelperUtility::getImportFolderPath().'pages/';
		$files = array_diff(scandir($dir), array('.', '..'));
		$cliArguments = $this->cliArguments;
		unset($cliArguments[0], $cliArguments[1]);
		if ($cliArguments) {
			foreach ($files as $key => $d) {
				if (!in_array($d, $cliArguments)) {
					unset($files[$key]);
				}
			}
		}
		foreach ($files as $filename) {
			$pageName = str_replace('.html', '', $filename);
			if ($this->debugOutput) {
				echo $pageName;
			}
			$filePath = $dir.$pageName.'.html';
			MediaWikiUtility::saveFileToPage(
				$pageName,
				$filePath
			);
			if ($this->debugOutput) {
				echo " - done\n";
			}
		}
	}

	private function createAlternateContentTempFile($pageName, $content) {
		$tempDir = HelperUtility::getTempFolderPath().'page/';
		if (!file_exists($tempDir)) {
			mkdir($tempDir);
		}
		$filePath = $tempDir.$pageName.'.html';
		file_put_contents($filePath, $content);

		return $filePath;
	}
}