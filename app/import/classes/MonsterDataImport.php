<?php

class MonsterDataImport extends AbstractDataImport {

	use SpawnImport;
	use LocationImport;


	protected function getData() {
		return DataBag::getBag()->getMonsterData();
	}

	protected function getNamingIdentifier() {
		return NamingUtility::IDENTIFIER_MONSTER;
	}

	protected function getContent($monster) {
		$contents =  [];
		$dropResults = $this->getDropsResult($monster);
		if ($dropResults) {
			$contents[] =  "== Drops ==\n\n".implode("\n", $dropResults);
		}
		$experienceTableRows = $this->getExperienceTableRows($monster);
		if ($experienceTableRows) {
			$contents[] = "== Experience Table ==\n\n".implode("\n", $experienceTableRows);
		}
		$spawnRows = $this->getSpawnRows($monster->spawns);
		if ($spawnRows) {
			$contents[] = "== Spawns ==\n\n".implode("\n", $spawnRows);
		}
		if($monster->rank == 'violet') {
			$locationRows = $this->getLocationRows([$monster->location]);
			if ($locationRows) {
				$contents[] = "== Location ==\n\n".implode("\n", $locationRows);
			}
		}
		$attackRows = $this->getAttackRows($monster);
		if ($attackRows) {
			$contents[] = "== Attacks ==\n\n".implode("\n", $attackRows);
		}
		if($contents) {
			return parent::getContent($monster).implode("\n\n", $contents);
		}
	}

	protected function getAttackRows($item) {
		$rows = [];
		if($item->attacks) {
			foreach ($item->attacks as $attack) {
				$rows[] = '{{Attack|'.implode("|", [
						'monster={{PAGENAME}}',
						'minAttack='.$attack->minAttack,
						'maxAttack='.$attack->maxAttack,
						'attackRange='.$attack->attackRange,
						'target='.NamingUtility::transformApiValueToWikiName($attack->target),
						'triggerSkill='.($attack->triggerSkill ? DataBag::getBag()->getSkillIdsToNames()[$attack->triggerSkill] : ''),
						'triggerSkillLevel='.$attack->triggerSkillLevel,
						'triggerSkillProbability='.$attack->triggerSkillProbability,
					]).'}}';
			}
		}

		return $rows;
	}

	private function getDropsResult($monster) {
		$dropsResult = [];
		if ($monster->drops) {
			$monsterName = DataBag::getBag()->getMonsterIdsToNames()[$monster->id];
			if ($monsterName) {
				foreach ($monster->drops as $drop) {
					$itemName = DataBag::getBag()->getItemIdsToNames()[$drop->item];
					if ($itemName) {
						$probRange = explode(";", str_replace(["[", "]", "%", "<"], "", $drop->probabilityRange));
						if (!$probRange[1]) {
							$probRange[1] = $probRange[0];
							$probRange[0] = 0;
						}
						$dropsResult[] = '{{drop|'.implode("|", [
								'item='.$itemName,
								'probabilityMin='.$probRange[0],
								'probabilityMax='.$probRange[1],
								'common='.($drop->common ? 1 : 0),
							]).'}}';
					}
				}
			}
		}

		return $dropsResult;
	}

	private function getExperienceTableRows($monster) {
		$expTableRows = [];
		if ($monster->experienceTable) {
			foreach ($monster->experienceTable as $level => $exp) {
				if ($exp > 0) {
					$expTableRows[] = '{{Experience|level='.($level + 1).'|experience='.$exp.'}}';
				}
			}
		}

		return $expTableRows;
	}
}