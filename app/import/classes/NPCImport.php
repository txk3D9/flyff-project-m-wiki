<?php

class NPCImport extends AbstractApiImport {

	protected $propertiesHandledToList = ['menus'];
	protected $unhandledProperties = ['locations', 'shop'];

	function getCategories($item): array {
		$categories = ['NPC'];

		return $categories;
	}

	protected function getNamingIdentifier() {
		return NamingUtility::IDENTIFIER_NPC;
	}
}