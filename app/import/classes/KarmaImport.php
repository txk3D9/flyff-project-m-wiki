<?php

class KarmaImport extends AbstractApiImport {
	protected $passthroughProperties = ['nameColor'];

	function execute() {
		$data = $this->getApiJsonData();
		foreach ($data as $item) {
			$itemResults[] = $this->getItemResult($item);
		}

		$karmaRows = "<!--\n";
		foreach($itemResults as $itemResult) {
			$karmaRows .= "-->{{Karma Row|".implode("|",$itemResult)."}}<!--\n";
		}
		$karmaRows .= '-->';

		$content = str_replace('$karmaRows',
			$karmaRows,
			file_get_contents(HelperUtility::getImportFolderPath().'templates/data/karma.html'));

		$this->saveContentToPage($content, 'Karma');
	}

	function handleProperty($propertyName, $propertyValue, $item) {
		if($propertyName == 'nameColor') {
			//https://stackoverflow.com/questions/32962624/convert-rgb-to-hex-color-values-in-php/40761010
			return $color = sprintf("#%02x%02x%02x", $propertyValue->r, $propertyValue->g, $propertyValue->b);
		}

		return parent::handleProperty($propertyName, $propertyValue, $item);
	}

	function getCategories($item): array {
		return [];
	}
}