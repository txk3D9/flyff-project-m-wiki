<?php

class MonsterImport extends AbstractApiImport {

	protected $specialyHandledProperties = ['experienceTable', 'attacks', 'drops', 'spawns'];
	protected $propertiesHandledToList = ['summoned'];
	protected $allowedRanksAsCategory = ['super', 'boss', 'giant'];
	protected $apiValueToWikiNameProperties = ['area', 'element', 'rank', 'experienceSharing'];

	function getCategories($item): array {
		$categories = ['Monster'];
		if ($item->rank && in_array($item->rank, $this->allowedRanksAsCategory)) {
			$categories[] = ucfirst($item->rank);
		}

		return $categories;
	}

	function handlePropertyToList($propertyName, $propertyValue){
		if($propertyName == 'summoned') {
			$names = [];
			foreach($propertyValue as $monsterId) {
				$name = DataBag::getBag()->getMonsterIdsToNames()[$monsterId];
				if($name) {
					$names[] = '[['.$name.']]';
				}
			}

			return parent::handlePropertyToList($propertyName, $names);
		} else {
			return parent::handlePropertyToList($propertyName, $propertyValue);
		}
	}
}