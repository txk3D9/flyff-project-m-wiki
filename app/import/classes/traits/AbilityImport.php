<?php

trait AbilityImport {

	protected function getAbilityRows($item, $additionalProperties = '') {
		$rows = [];
		if ($item->abilities) {
			foreach ($item->abilities as $ability) {
				$rows[] = '{{Ability|'.implode("|", [
						$additionalProperties,
						'parameter='.NamingUtility::getUniqueName(NamingUtility::transformApiValueToWikiName($ability->parameter), null, NamingUtility::IDENTIFIER_ABILITY),
						'add='.$ability->add,
						'set='.$ability->set,
						'rate='.($ability->rate ? 1 : 0),
						'attribute='.NamingUtility::transformApiValueToWikiName($ability->attribute),
						'dotValue='.$ability->dotValue,
						'dotMode='.NamingUtility::transformApiValueToWikiName($ability->dotMode),
						'skillChanceSkill='.$ability->skill,
						'skillChanceLevel='.$ability->skillLevel,
						'pvp='.$ability->pve,
						'pve='.$ability->pvp,
					]).'}}';
			}
		}

		return $rows;
	}
}