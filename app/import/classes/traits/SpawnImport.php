<?php

trait SpawnImport {

	protected function getSpawnRows($spawns) {
		$rows = [];
		if ($spawns) {
			if(is_object($spawns)) {
				$spawns = [$spawns];
			}
			foreach ($spawns as $spawn) {
				$rows[] = '{{Spawn|'.implode("|", [
						'world='.DataBag::getBag()->getWorldIdsToNames()[$spawn->world],
						'left='.$spawn->left,
						'right='.$spawn->right,
						'top='.$spawn->top,
						'bottom='.$spawn->bottom,
						'continent='.DataBag::getBag()->getContinentIdsToNames()[$spawn->continent],
						'aggressivity='.$spawn->aggressivity,
					]).'}}';
			}
		}

		return $rows;
	}
}