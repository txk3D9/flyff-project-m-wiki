<?php

trait LocationImport {

	protected function getLocationRows($locations, $extraRows = []) {
		$rows = [];
		if ($locations) {
			if(is_object($locations)) {
				$locations = [$locations];
			}
			foreach ($locations as $location) {
				$rows[] = '{{Location|'.implode("|", array_merge([
						'world='.DataBag::getBag()->getWorldIdsToNames()[$location->world],
						'x='.$location->x,
						'y='.$location->y,
						'z='.$location->z,
						'continent='.DataBag::getBag()->getContinentIdsToNames()[$location->continent],
					], $extraRows)).'}}';
			}
		}

		return $rows;
	}
}