<?php

class QuestImport extends AbstractApiImport {

	protected $specialyHandledProperties = ['endReceiveExperience', 'endReceiveItems', 'endKillMonster', 'endNeededItems', 'endRemoveItems'];
	protected $propertiesHandledToList = ['beginClasses'];
	protected $apiValueToWikiNameProperties = ['type'];
	protected $localizationPropertiesHandledToString = ['name', 'description', 'descriptionComplete'];

	function getCategories($item): array {
		return ['Quest'];
	}

	function handleProperty($propertyName, $propertyValue, $item) {
		if (in_array($propertyName, ['beginNPC', 'endNPC', 'endTalkNPC'])) {
			return DataBag::getBag()->getNPCIdsToNames()[$propertyValue];
		} else if ($propertyName == 'parent') {
			return DataBag::getBag()->getQuestIDsToNames()[$propertyValue];
		} else if ($propertyName == 'type') {
			if ($item->maxLevel == 120
				&& $item->endNeededItems && count($item->endNeededItems) === 1
				&& DataBag::getBag()->getItemData()[$item->endNeededItems[0]->item]->category == "booty"
				&& $item->beginQuests == null) {
				return "booty";
			}
		} else if (in_array($propertyName, ['dialogsBegin',
											'dialogsAccept', 'dialogsDecline',
											'dialogsComplete', 'dialogsFail'])) {
			$dialogs = [];
			foreach($propertyValue as $dialog) {
				$dialogs[] = TextReplacementUtility::replaceTextWithLinkMarkers($dialog->en);
			}

			return implode(" ", $dialogs);
		}

		return parent::handleProperty($propertyName, $propertyValue, $item);
	}

	protected function additionalProperties($item): array {
		return [
			'hasItemReward' => ($item->endReceiveItems) ? 1 : 0,
			'hasEndVisitPlace' => ($item->endVisitPlace) ? 1 : 0,
			'endReceiveExperience' => ($item->endReceiveExperience) ? $item->endReceiveExperience[$item->minLevel - 1] : null,
		];
	}

	function handlePropertyToList($propertyName, $propertyValue) {
		$propertyNameToData = [
			'beginClasses' => DataBag::getBag()->getClassIdsToNames(),
		];
		if (array_key_exists($propertyName, $propertyNameToData)) {
			$names = [];
			foreach ($propertyValue as $monsterId) {
				$name = $propertyNameToData[$propertyName][$monsterId];
				if ($name) {
					$names[] = '[['.$name.']]';
				}
			}

			return parent::handlePropertyToList($propertyName, $names);
		} else {
			return parent::handlePropertyToList($propertyName, $propertyValue);
		}
	}

	function getExtraTemplateReplacements($item) {
		return [
			[
				'$description'
			],
			[
				$item->description->en && $item->description->en != "null"
					? TextReplacementUtility::replaceTextWithLinkMarkers($item->description->en) : '',
			],
		];
	}
}
