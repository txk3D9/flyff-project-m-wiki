<?php

class TextReplacement {

	public $name;
	public $value;

	public function __construct($name, $value = null) {
		$this->name = $name;
		$this->value = ($value === null) ? MediaWikiUtility::escapePageNameKeepWhitespaces($name) : MediaWikiUtility::escapePageNameKeepWhitespaces($value);
	}
}