<?php

class WorldImport extends AbstractApiImport {

	protected $specialyHandledProperties = ['places', 'lodestars', 'continents'];

	function getCategories($item): array {
		$categories = ['World'];
		if ($item->type == 'dungeon') {
			$categories[] = 'Dungeon';
		}

		return $categories;
	}

	function handleProperty($propertyName, $propertyValue, $item) {
		if ($propertyName == 'revivalWorld') {
			return DataBag::getBag()->getWorldIdsToNames()[$propertyValue];
		} else if ($propertyName == 'revivalKey') {
			return $propertyValue;
		} else {
			return parent::handleProperty($propertyName, $propertyValue, $item);
		}
	}
}