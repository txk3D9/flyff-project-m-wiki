<?php

class MediaWikiUtility {

	static function saveFileToPage($pageName, $filePath) {
		$command = 'php '.MediaWikiUtility::getMaintenanceFolder().'edit.php -u "Solakram" '.escapeshellcmd($pageName).' < '.escapeshellcmd($filePath);
		exec($command,$output);
	}

	static function getPageContent($pageName) {
		$command = 'php '.MediaWikiUtility::getMaintenanceFolder().'getText.php '.escapeshellcmd($pageName);
		exec($command, $output);
		return implode("\n", $output);
	}

	static function recreateCargoTable($table) {
		exec("php ".MediaWikiUtility::getExtensionFolder()."Cargo/maintenance/cargoRecreateData.php --table ".escapeshellcmd($table)." --quiet");
	}

	static function importImageFromFolder($folder, $overwrite = true) {
		$command = "php ".MediaWikiUtility::getMaintenanceFolder()."importImages.php --user='Solakram' ".escapeshellcmd($folder);
		if($overwrite) {
			$command .= " --overwrite";
		}
		exec($command);
	}

	static function getMaintenanceFolder() {
		return dirname(__FILE__).'/../../html/maintenance/';
	}

	static function getExtensionFolder() {
		return dirname(__FILE__).'/../../html/extensions/';
	}

	static function escapePageName($pageName) {
		return str_replace(" ", "_", MediaWikiUtility::escapePageNameKeepWhitespaces($pageName));
	}

	static function escapePageNameKeepWhitespaces($pageName) {
		//https://www.mediawiki.org/wiki/Manual:Page_title
		return str_replace(["#", "<", ">", "[", "]", "|", "{", "}", "_"], "", $pageName);
	}
}