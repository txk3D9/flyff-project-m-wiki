<?php

class ContinentImport extends AbstractApiImport {

	function getCategories($item): array {
		return ['Continent'];
	}

	protected function getItemResult($item) {
		return [
			'id='.$item->id,
			'world='.$item->world,
			'town='.($item->town ? 1 : 0)
		];
	}

	function getApiJsonData(): array {
		return DataBag::getBag()->getContinentData();
	}
}