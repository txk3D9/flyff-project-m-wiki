<?php

class SkillDataImport extends AbstractDataImport {

	use AbilityImport;

	protected function getData() {
		return DataBag::getBag()->getSkillData();
	}

	protected function getNamingIdentifier() {
		return NamingUtility::IDENTIFIER_SKILL;
	}

	protected function getContent($skill) {
		$contents = [];
		$levelRows = $this->getLevelRows($skill);
		if ($levelRows) {
			$contents[] = "== Skill Levels ==\n\n".implode("\n", $levelRows);
		}
		$abilityRows = $this->getAbilityRowsConsiderLevels($skill);
		if ($abilityRows) {
			$contents[] = "== Skill Level Abilities ==\n\n".implode("\n", $abilityRows);
		}
		$scalingRows = $this->getScalingRows($skill);
		if ($scalingRows) {
			$contents[] = "== Skill Level Scaling ==\n\n".implode("\n", $scalingRows);
		}
		if ($contents) {
			return parent::getContent($skill).implode("\n\n", $contents);
		}
	}

	protected function getAbilityRowsConsiderLevels($skill) {
		$rows = [];
		foreach ($skill->levels as $num => $level) {
			$rows = array_merge($rows, $this->getAbilityRows($level, '|skillLevel='.($num + 1)));
		}

		return $rows;
	}

	protected function getLevelRows($skill) {
		$rows = [];
		if ($skill->levels) {
			foreach ($skill->levels as $num => $skillLevel) {
				$rows[] = '{{Skill Level|'.implode("|", [
						'skill={{PAGENAME}}',
						'level='.($num + 1),
						'minAttack='.$skillLevel->minAttack,
						'maxAttack='.$skillLevel->maxAttack,
						'probability='.$skillLevel->probability,
						'probabilityPVP='.$skillLevel->probabilityPVP,
						'consumedMP='.$skillLevel->consumedMP,
						'consumedFP='.$skillLevel->consumedFP,
						'cooldown='.$skillLevel->cooldown,
						'casting='.$skillLevel->casting,
						'duration='.$skillLevel->duration,
						'durationPVP='.$skillLevel->durationPVP,
						'dotTick='.$skillLevel->dotTick,
						'spellRange='.$skillLevel->spellRange,
						'wallLives='.$skillLevel->wallLives,
						'reflectedDamagePVE='.$skillLevel->reflectedDamagePVE,
						'refletectedDamagePVP='.$skillLevel->refletectedDamagePVP,
					]).'}}';
			}
		}

		return $rows;
	}

	protected function getScalingRows($skill) {
		$rows = [];
		if ($skill->levels) {
			foreach ($skill->levels as $num => $skillLevel) {
				if($skillLevel->scalingParameters) {
					foreach ($skillLevel->scalingParameters as $scaling) {
						$rows[] = '{{Scaling|'.implode("|", [
								'skill={{PAGENAME}}',
								'skillLevel='.($num + 1),
								'parameter='.NamingUtility::transformApiValueToWikiName($scaling->parameter),
								'stat='.NamingUtility::transformApiValueToWikiName($scaling->stat),
								'scale='.$scaling->scale,
								'pve='.($scaling->pve ? 1 : 0),
								'pvp='.($scaling->pvp ? 1 : 0),
							]).'}}';
					}
				}

			}
		}

		return $rows;
	}
}