<?php

class AchievementImport extends AbstractApiImport {

	protected $specialyHandledProperties = ['levels'];
	protected $propertiesHandledToList = ['monsters', 'items', 'skills', 'classes'];
	protected $apiValueToWikiNameProperties = ['type', 'category'];

	function getCategories($item): array {
		return ['Achievement'];
	}

	function handleProperty($propertyName, $propertyValue, $item){
		if($propertyName == 'mainMonster'){
			return DataBag::getBag()->getMonsterIdsToNames()[$propertyValue];
		}
		if($propertyName == 'mainItem'){
			return DataBag::getBag()->getItemIdsToNames()[$propertyValue];
		}
		if($propertyName == 'mainSkill'){
			return DataBag::getBag()->getSkillIdsToNames()[$propertyValue];
		}
		if($propertyName == 'mainClass'){
			return DataBag::getBag()->getClassIdsToNames()[$propertyValue];
		}

		return parent::handleProperty($propertyName, $propertyValue, $item);
	}

	function handlePropertyToList($propertyName, $propertyValue){
		$propertyNameToData = [
//			'monsters' => DataBag::getBag()->getMonsterIdsToNames(), //not in API anymore
			'items' => DataBag::getBag()->getItemIdsToNames(),
			'skills' => DataBag::getBag()->getSkillIdsToNames(),
			'classes' => DataBag::getBag()->getClassIdsToNames(),
		];
		if(array_key_exists($propertyName, $propertyNameToData)) {
			$names = [];
			foreach($propertyValue as $monsterId) {
				$name = $propertyNameToData[$propertyName][$monsterId];
				if($name) {
					$names[] = '[['.$name.']]';
				}
			}
			return parent::handlePropertyToList($propertyName, $names);
		} else {
			return parent::handlePropertyToList($propertyName, $propertyValue);
		}
	}

	function getExtraTemplateReplacements($item) {
		return [
			[
				'$description'
			],
			[
			$item->description->en && $item->description->en != "null"
				? TextReplacementUtility::replaceTextWithLinkMarkers($item->description->en) : '',
			],
		];
	}
}
