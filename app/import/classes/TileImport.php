<?php

class TileImport extends IconImport {

	protected $identifier;

	function __construct($identifier = 'tiles') {
		$this->identifier = $identifier;
	}

	protected function getCategories() {
		return ['Tile'];
	}
}