<?php

abstract class AbstractImport {

	protected $keepTempFiles = true;
	protected $debugOutput = true;
	protected $cliArguments = null;


	function setCliArguments($arguments) {
		$this->cliArguments = $arguments;
	}

	function run() {
		if ($this->debugOutput) {
			echo "Import start: ".get_class($this)."\n";
		}
		$this->execute();
		if ($this->debugOutput) {
			echo "Import end: ".get_class($this)."\n----------------------\n";
		}
	}

	abstract function execute();

	protected function savePage($pageName, $content) {
		$pageName = MediaWikiUtility::escapePageName($pageName);
		$dir = HelperUtility::getTempFolderPath().'data/';
		if (!file_exists($dir)) {
			mkdir($dir);
		}
		$filePath = $dir.$pageName.'.html';
		file_put_contents($filePath, $content);
		MediaWikiUtility::saveFileToPage(
			$pageName,
			$filePath
		);
		if (!$this->keepTempFiles) {
			unlink($filePath);
		}
	}

	/**
	 * @return bool
	 */
	public function isKeepTempFiles(): bool {
		return $this->keepTempFiles;
	}

	/**
	 * @param bool $keepTempFiles
	 */
	public function setKeepTempFiles(bool $keepTempFiles): void {
		$this->keepTempFiles = $keepTempFiles;
	}

	/**
	 * @return bool
	 */
	public function isDebugOutput(): bool {
		return $this->debugOutput;
	}

	/**
	 * @param bool $debugOutput
	 */
	public function setDebugOutput(bool $debugOutput): void {
		$this->debugOutput = $debugOutput;
	}
}