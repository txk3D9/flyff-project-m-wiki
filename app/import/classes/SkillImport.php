<?php

class SkillImport extends AbstractApiImport {

	protected $specialyHandledProperties = ['levels'];
	protected $passthroughProperties = ['requirements'];
	protected $apiValueToWikiNameProperties = ['weapon', 'element', 'target', 'combo'];

	function getCategories($item): array {
		$categories = ['Skill'];
		$class = $this->getClassNameOfObject($item);
		if ($class) {
			$categories[] = 'Skill '.$class;
		}

		return $categories;
	}

	function handleProperty($propertyName, $propertyValue, $item) {
		if ($propertyName == 'class') {
			return DataBag::getBag()->getClassIdsToNames()[$propertyValue];
		}
		if ($propertyName == 'consumedItem') {
			return DataBag::getBag()->getItemIdsToNames()[$propertyValue];
		}
		if ($propertyName == 'triggerSkill') {
			return DataBag::getBag()->getSkillIdsToNames()[$propertyValue];
		}
		if ($propertyName == 'requirements') {
			$requirements = [];
			foreach ($propertyValue as $requirement) {
				$requirements[] = "[[".DataBag::getBag()->getSkillIdsToNames()[$requirement->skill]."]] level ".$requirement->level;
			}

			return implode("<br />", $requirements);
		}

		return parent::handleProperty($propertyName, $propertyValue, $item);
	}

	function getExtraTemplateReplacements($item) {
		return [
			['$skillLevelsTable', '$description'],
			[
				$this->getSkillLevelsTable($item),
				TextReplacementUtility::replaceTextWithLinkMarkers($item->description->en),
			],
		];
	}

	private function getSkillLevelsTable($item) {
		$paramRows = [];
		if ($item->levels) {
			foreach ($item->levels as $level) {
				foreach ($level as $paramName => $paramValue) {
					if ($paramValue) {
						if($paramName == 'minAttack') {
							$paramName = 'attack';
						} else if($paramName == 'maxAttack') {
							continue;
						}
						$paramRows[$paramName] = "|".$paramName."=1";
					}
				}
			}
		}
		if ($paramRows) {
			return "{{Skill Levels Table".implode("\n", $paramRows)."}}";
		} else {
			return "";
		}
	}
}
