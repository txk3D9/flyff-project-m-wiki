<?php

$mandatoryFiles = ['AbstractImport.php'];

$folders = [dirname(__FILE__).'/classes/traits/', dirname(__FILE__).'/classes/'];

foreach($folders as $folder) {
	$files = array_diff(
		scandir($folder),
		array('.', '..', 'traits')
	);
	$filesToRequire = array_merge($mandatoryFiles, $files);
	foreach($filesToRequire as $n => $file) {
		if(file_exists($folder.$file)) {
			require_once $folder.$file;
		}
	}
}
