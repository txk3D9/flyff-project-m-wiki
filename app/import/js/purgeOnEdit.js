/*
https://phabricator.wikimedia.org/T295732
https://nookipedia.com/w/index.php?title=MediaWiki:Gadget-PurgeOnEdit.js&action=edit

Automatically purge page cache after editing a page.
This is a workaround for Cargo template queries
not rendering HTML after a page edit.
*/

if(mw.config.get('wgPostEdit') === 'saved') {
	var api = new mw.Api();
	api.post({
		action: 'purge',
		titles: mw.config.get('wgPageName')
	}).done(function(data) {
		console.log('Page cache purged.');
	});
}
