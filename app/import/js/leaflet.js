(function() {
	let leafletMaps = document.getElementsByClassName('leaflet-map');
	var leafletMap;
	for (leafletMap of leafletMaps) {
		let data = JSON.parse(leafletMap.textContent);
		data = transformData(data);
		leafletMap.textContent = null;

		leafletMap.id = data.tileName + "_" + new Date().valueOf();
		data.width = data.width;
		data.height = data.height;

		leafletMap.style.display = "block";

		let map = createMap(leafletMap, data);
		if (data.spawns.length > 0) {
			drawSpawns(map, data);
		} else if (data.locations.length > 0) {
			drawLocations(map, data);
		}
	}

	function transformData(data) {
		let finalData;

		for (const d of data) {
			if (finalData == null) {
				finalData = {
					world: d.world, tileSize: d.tileSize, tileName: d.tileName,
					height: d.height, width: d.width, spawns: [], locations: []
				};
			}
			if (d.top) { // is spawn
				finalData.spawns.push({top: d.top, posLeft: d.posLeft, posRight: d.posRight, bottom: d.bottom})
			} else if (d.x) { // is location
				finalData.locations.push({x: d.x, y: d.y, z: d.z})
			}
		}

		return finalData;
	}

	function createMap(domElement, data) {
		// let tile_url = window.location.origin + '/wiki/Special:Redirect/file/File:' + data.tileName + '{x}-{y}-0.png';
		let tile_url = window.location.origin + '/images/tiles/' + data.tileName + '{x}-{y}-0.png';
		let map = L.map(domElement.id,
			{
				crs: L.CRS.Simple,
				center: new L.LatLng(-(data.height / 4), data.width / 4),
				zoomSnap: 0.25,
				zoom: -2,
				minZoom: -3,
				maxZoom: 1,
			}
		);

		L.tileLayer(tile_url, {
			id: data.tileName,
			attribution: '&copy; 2021 Gala Lab Corp.',
			tileSize: 256, //parseInt(data.tileSize)
			minZoom: -3,
			maxZoom: 1,
			useCache: true,
			minNativeZoom: 0,
			maxNativeZoom: 0,
			bounds: [[0, 0], [-(data.height / 2), data.width / 2]],
			noWrap: true,
			crossOrigin: "anomynous",
		}).addTo(map);

		if (data.spawns.length > 0 || data.locations.length > 0) {
			// add world name before map
			let link = '<a href="' + window.location.origin + '/wiki/' + encodeURIComponent(data.world) + '">' + data.world + '</a>';
			domElement.insertAdjacentHTML('beforeBegin', link);
		} else {
			map.fitBounds([[0, 0], [-data.height/2, data.width/2]])
		}
		return map;
	}

	function drawSpawns(map, data) {
		var maxTop;
		var maxLeft;
		var maxBottom;
		var maxRight;

		for (const spawn of data.spawns) {
			let top = (spawn.top - data.height) / 2
			let left = spawn.posLeft / 2;
			let right = spawn.posRight / 2
			let bottom = (spawn.bottom - data.height) / 2;

			if (maxTop == null || maxTop > top) {
				maxTop = top;
			}
			if (maxLeft == null || maxLeft > left) {
				maxLeft = left;
			}
			if (maxBottom == null || maxBottom < bottom) {
				maxBottom = bottom;
			}
			if (maxRight == null || maxRight < right) {
				maxRight = right;
			}

			L.rectangle([[top, left], [bottom, right]], {color: 'blue', weight: 1}).addTo(map);
		}

		let fitBoundsPadding = 150;
		map.fitBounds([[maxTop - fitBoundsPadding, maxLeft - fitBoundsPadding], [maxBottom + fitBoundsPadding, maxRight + fitBoundsPadding]])
	}

	function drawLocations(map, data) {
		var maxZ;
		var minZ;
		var maxX;
		var minX;

		for (const location of data.locations) {
			let z = (location.z - data.height) / 2
			let x = (location.x) / 2;

			if (maxZ == null || maxZ > z) {
				maxZ = z;
			}
			if (minZ == null || minZ < z) {
				minZ = z;
			}
			if (maxX == null || maxX < x) {
				maxX = x;
			}
			if (minX == null || minX > x) {
				minX = x;
			}

			L.rectangle([[z - 1, x - 1], [z + 1, x + 1]], {color: 'blue', weight: 5}).addTo(map);
		}

		let fitBoundsPadding = 150;
		map.fitBounds([[maxZ - fitBoundsPadding, maxX - fitBoundsPadding], [minZ + fitBoundsPadding, minX + fitBoundsPadding]])
	}
})();

