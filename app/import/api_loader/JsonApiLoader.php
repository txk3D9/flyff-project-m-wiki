<?php

abstract class JsonApiLoader extends ApiLoader {

	public function execute() {
		$this->checkDependencies();
		$data = $this->getData();
		$this->saveToJson($this->getJsonFileName(), $data);
	}

	function getStatus() {
		try {
			$currentData = $this->getCurrentJsonData();
			$dataFromApi = $this->getData();
			if ($currentData == $dataFromApi) {
				return ApiLoader::STATUS_COMPLETE;
			}

			return ApiLoader::STATUS_OUTDATED;
		} catch (\Exception $e) {
			return ApiLoader::STATUS_MISSING;
		}
	}

	abstract function getJsonFileName(): string;

	abstract function getData(): array;
}