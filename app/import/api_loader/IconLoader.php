<?php

class IconLoader extends ApiLoader {

	/**
	 * @var IdListLoader|null $idListLoader
	 */
	protected ?IdListLoader $idListLoader;
	protected string $identifier;
	protected string $urlAppendix;
	protected string $prefixFilename;

	public function __construct(IdListLoader $idListLoader = null, string $identifier = '', $urlAppendix = '', $prefixFilename = '') {
		$this->idListLoader = $idListLoader;
		$this->identifier = $identifier;
		$this->urlAppendix = ($urlAppendix) ? $urlAppendix : $identifier;
		$this->prefixFilename = $prefixFilename;
	}

	public function execute() {
		$this->checkDependencies();
		$iconNames = $this->getIconNameList();
		foreach ($iconNames as $iconName) {
			$image = $this->getIconContent($iconName);
			$this->saveImage($iconName, $image);
		}
	}

	function getIconContent($iconName) {
		return $this->makeRequest('image/'.$this->urlAppendix.'/'.$iconName);
	}

	function saveImage($fileName, $iconContent) {
		if($fileName && $iconContent) {
			file_put_contents( $this->getSaveDir().$this->prefixFilename.$fileName, $iconContent);
		}
	}

	function getIconNameList() {
		$iconNames = [];
		foreach ($this->idListLoader->getCurrentJsonData() as $item) {
			$icon = ($item->icon) ? $item->icon : $item->image;
			if ($icon) {
				$iconNames[$icon] = $icon;
			}
		}

		return $iconNames;
	}

	function getStatus() {
		$iconNames = $this->getIconNameList();
		$numOfAvailablesIcons = 0;
		foreach($iconNames as $iconName) {
			if(file_exists($this->getSaveDir().$iconName)) {
				$numOfAvailablesIcons++;
			}
		}

		if($numOfAvailablesIcons == count($iconNames)) {
			return ApiLoader::STATUS_COMPLETE;
		} else if ($numOfAvailablesIcons < count($iconNames)) {
			return ApiLoader::STATUS_INCOMPLETE;
		} else if ($numOfAvailablesIcons == 0) {
			return ApiLoader::STATUS_MISSING;
		}
	}

	function checkDependencies() {
		if (!$this->idListLoader->getCurrentJsonData()) {
			throw new \Exception(get_class($this).' dependencies not complete');
		}
	}

	function getSaveDir() {
		$saveDir = HelperUtility::getImportFolderPath().'/api_icons/'.$this->identifier.'/';
		if(!file_exists($saveDir)) {
			mkdir($saveDir);
		}
		return $saveDir;
	}
}