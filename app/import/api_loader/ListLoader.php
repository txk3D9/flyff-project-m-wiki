<?php

class ListLoader extends JsonApiLoader {

	protected string $urlAppendix;
	protected string $jsonFileName;

	public function __construct(string $urlAppendix) {
		$this->urlAppendix = $urlAppendix;
		$this->jsonFileName = $urlAppendix;
	}

	function checkDependencies() {
	}

	function getData(): array {
		$request = $this->makeRequest($this->urlAppendix.'/');

		return json_decode($request);
	}

	function getJsonFileName(): string {
		return $this->jsonFileName;
	}
}