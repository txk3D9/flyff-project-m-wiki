<?php

class VersionLoader extends JsonApiLoader {

	function checkDependencies() {}

	function getData() : array {
		return [$this->makeRequest('version/data')];
	}

	function getJsonFileName() : string {
		return 'version';
	}

	function getStatus() {
		try {
			$currentData = $this->getCurrentJsonData();
			$dataFromApi = $this->getData();
			if($currentData == $dataFromApi) {
				return ApiLoader::STATUS_COMPLETE;
			}

			return ApiLoader::STATUS_OUTDATED;
		} catch (\Exception $e) {
			return ApiLoader::STATUS_MISSING;
		}
	}
}