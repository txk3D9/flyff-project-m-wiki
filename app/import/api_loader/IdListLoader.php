<?php

class IdListLoader extends JsonApiLoader {

	/**
	 * @var IdsLoader $idsLoader
	 */
	protected IdsLoader $idsLoader;
	protected string $urlAppendix;
	protected string $jsonFileName;
	protected bool $removeNonEnglishTranslations = true;

	public function __construct(IdsLoader $idsLoader, string $urlAppendix) {
		$this->idsLoader = $idsLoader;
		$this->urlAppendix = $urlAppendix;
		$this->jsonFileName = $urlAppendix;
	}

	function checkDependencies() {
		if ($this->idsLoader->getStatus() != ApiLoader::STATUS_COMPLETE) {
			throw new \Exception(get_class($this).' loader dependencies not complete');
		}
	}

	function getData(): array {
		$data = [];
		$ids = $this->idsLoader->getCurrentJsonData();
		$batchSize = 300;
		foreach (array_chunk($ids, $batchSize) as $chunkedIds) {
			$request = $this->makeRequest($this->urlAppendix.'/'.implode(',', $chunkedIds));
			$jsonedRequest = json_decode($request);
			if($this->removeNonEnglishTranslations && $jsonedRequest) {
				foreach($jsonedRequest as $item) {
					$this->removeNonEnglishTranslations($item);
				}
			}
			$data = array_merge($data, $jsonedRequest);
		}

		return $data;
	}

	function removeNonEnglishTranslations($object) {
		if(is_object($object)) {
			if(property_exists($object, 'en')) {
				foreach($object as $langNum => $lang) {
					if($langNum != 'en') {
						unset($object->$langNum);
					}
				}
			} else {
				foreach($object as $c) {
					$this->removeNonEnglishTranslations($c);
				}
			}
		} else if (is_array($object)) {
			foreach($object as $c) {
				$this->removeNonEnglishTranslations($c);
			}
		}
	}

	function getJsonFileName(): string {
		return $this->jsonFileName;
	}
}