<?php

class TilesLoader extends ApiLoader {

	/**
	 * @var IdListLoader|null $idListLoader
	 */
	protected ?IdListLoader $idListLoader;

	public function __construct(IdListLoader $idListLoader = null) {
		$this->idListLoader = $idListLoader;
	}

	public function execute() {
		$this->checkDependencies();
		foreach ($this->idListLoader->getCurrentJsonData() as $world) {
			$worldTile = $world->tileName;
			$cols = $world->width / $world->tileSize;
			$rows = $world->height / $world->tileSize;
			for ($x = 0; $x < $cols; $x++) {
				for ($y = 0; $y < $rows; $y++) {
					$fileName = $worldTile.$x.'-'.$y.'-0'.'.png';
					$image = $this->getTileContent($fileName);
					$this->saveImage($fileName, $image);
				}
			}
//			$this->createCompleteImageFromTiles($world, $cols, $rows);
		}
	}

	function createCompleteImageFromTiles($world, $cols, $rows) {
		$worldTile = $world->tileName;
		$imageSize = 256;
		$completeImage = imagecreatetruecolor($cols*$imageSize, $rows*$imageSize);
		for ($x = 0; $x < $cols; $x++) {
			for ($y = 0; $y < $rows; $y++) {
				$fileName = $worldTile.$x.'-'.$y.'-0'.'.png';
				$localImage = $this->getSaveDir().$fileName;
				$src = imagecreatefrompng($localImage);
				imagecopy($completeImage, $src,  $imageSize*$x, $imageSize*$y, 0, 0, $imageSize, $imageSize);
			}
		}
		imagepng($completeImage, $this->getSaveDir().''.$worldTile.'-merged.png');
	}

	function getTileContent($fileName) {
		return $this->makeRequest('image/world/'.$fileName);
	}

	function saveImage($fileName, $tileContent) {
		if ($fileName && $tileContent) {
			file_put_contents($this->getSaveDir().$fileName, $tileContent);
		}
	}

	function getStatus() {
		return ApiLoader::STATUS_UNKNOWN;
	}

	function checkDependencies() {
		if (!$this->idListLoader->getCurrentJsonData()) {
			throw new \Exception(get_class($this).' dependencies not complete');
		}
	}

	function getSaveDir() {
		$saveDir = HelperUtility::getImportFolderPath().'/api_icons/tiles/';
		if (!file_exists($saveDir)) {
			mkdir($saveDir);
		}

		return $saveDir;
	}
}