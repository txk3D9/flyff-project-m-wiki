<?php

class ElementIconLoader extends IconLoader {

	const ELEMENTS = ["fire", "water", "electricity", "wind", "earth", "none"];

	public function __construct($urlAppendix = '', $prefixFilename = '') {
		parent::__construct(null, 'element', $urlAppendix, $prefixFilename);
	}

	function getIconNameList() {
		$iconNames = [];
		foreach(ElementIconLoader::ELEMENTS as $element) {
			if($element != 'none') {
				$iconNames[] = $element.'.png';
			}
		}

		return $iconNames;
	}

	function checkDependencies() {}
}