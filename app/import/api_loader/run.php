<?php
require_once dirname(__FILE__).'/ApiLoader.php';
require_once dirname(__FILE__).'/JsonApiLoader.php';
require_once dirname(__FILE__).'/IdsLoader.php';
require_once dirname(__FILE__).'/IdListLoader.php';
require_once dirname(__FILE__).'/ListLoader.php';
require_once dirname(__FILE__).'/IconLoader.php';
require_once dirname(__FILE__).'/TilesLoader.php';
require_once dirname(__FILE__).'/ElementIconLoader.php';
require_once dirname(__FILE__).'/VersionLoader.php';
require_once dirname(__FILE__).'/../classes/HelperUtility.php';
if (!$argv[1]) {
	throw new \Exception('no arg defined');
}
$idsToList = ['monster', 'item', 'class', 'world', 'equipset', 'skill', 'npc', 'karma', 'partyskill', 'quest', 'achievement'];
$list = ['upgradelevelbonus'];
$icons = ['monster', 'item', 'npc']; //'class', 'skill', is handled seperately

$nameToClass = [
	'version' => new VersionLoader(),
];
foreach ($idsToList as $name) {
	$nameToClass[$name.'Ids'] = new IdsLoader($name);
	$nameToClass[$name] = new IdListLoader($nameToClass[$name.'Ids'], $name);
}
foreach ($list as $name) {
	$nameToClass[$name] = new ListLoader($name);
}
foreach ($icons as $name) {
	$nameToClass[$name.'Icons'] = new IconLoader($nameToClass[$name], $name);
}
$nameToClass['targetClassIcons'] = new IconLoader($nameToClass['class'], 'class', 'class/target', 'target_');
$nameToClass['messengerClassIcons'] = new IconLoader($nameToClass['class'], 'class', 'class/messenger', 'messenger_');
$nameToClass['oldFemaleClassIcons'] = new IconLoader($nameToClass['class'], 'class', 'class/old_female', 'old_female_');
$nameToClass['oldMaleClassIcons'] = new IconLoader($nameToClass['class'], 'class', 'class/old_male', 'old_male_');
$nameToClass['skillIcons'] = new IconLoader($nameToClass['skill'], 'skill', 'skill/colored', '');
$nameToClass['oldSkillIcons'] = new IconLoader($nameToClass['skill'], 'skill', 'skill/old', 'old_');

$nameToClass['partySkillIcons'] = new IconLoader($nameToClass['partyskill'], 'partyskill', 'skill/colored', '');
$nameToClass['oldPartySkillIcons'] = new IconLoader($nameToClass['partyskill'], 'partyskill', 'skill/old', 'old_');

$nameToClass['pcElementIcons'] = new ElementIconLoader('element/pc', 'element_pc_');
$nameToClass['mobileElementIcons'] = new ElementIconLoader('element/mobile', 'element_mobile_');
$nameToClass['masqueradeElementIcons'] = new ElementIconLoader('element/masquerade', 'element_masquerade_');
$nameToClass['tiles'] = new TilesLoader($nameToClass['world']);

if ($argv[1] == 'all') {
	foreach ($nameToClass as $name => $class) {
		$class->execute();
		echo $name." - done\n";
	}
} else if ($argv[1] == 'check-status') {
	if ($argv[2]) {
		/**
		 * @var ApiLoader $class
		 */
		$class = $nameToClass[$argv[2]];
		echo $argv[2].' - '.$class->getStatus()."\n";
	} else {
		foreach ($nameToClass as $name => $class) {
			echo $name.' - '.$class->getStatus()."\n";
		}
	}
} else {
	/**
	 * @var ApiLoader $class
	 */
	$class = $nameToClass[$argv[1]];
	$class->execute();
}





