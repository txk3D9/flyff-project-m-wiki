<?php

class IdsLoader extends JsonApiLoader {

	protected string $urlAppendix;
	protected string $jsonFileName;

	function checkDependencies() {}

	public function __construct(string $urlAppendix) {
		$this->urlAppendix = $urlAppendix;
		$this->jsonFileName = $urlAppendix.'_ids';
	}

	function getData() : array {
		return json_decode($this->makeRequest($this->urlAppendix));
	}

	function getJsonFileName(): string {
		return $this->jsonFileName;
	}
}