<?php

abstract class ApiLoader {

	private const BASE_URL = 'https://flyff-api.sniegu.fr/';
	const STATUS_COMPLETE = 'complete';
	const STATUS_INCOMPLETE = 'incomplete';
	const STATUS_MISSING = 'missing';
	const STATUS_UNKNOWN = 'unknown';
	const STATUS_OUTDATED = 'outdated';
	static $CURRENT_REQUST_NUMBER = 0;

	abstract public function execute();

	abstract function getStatus();

	abstract function checkDependencies();

	public function saveToJson(string $filename, array $content) {
		if (strpos($filename, '.json') === false) {
			$filename = $filename.'.json';
		}
		file_put_contents(HelperUtility::getImportFolderPath().'/api_json_files/'.$filename, json_encode($content));
	}

	function getCurrentJsonData(): array {
		$filename = $this->getJsonFileName();
		if (strpos($filename, '.json') === false) {
			$filename = $filename.'.json';
		}
		$file = HelperUtility::getImportFolderPath().'/api_json_files/'.$filename;
		if (file_exists($file)) {
			return json_decode(file_get_contents($file));
		} else {
			throw new \Exception('file does not exist');
		}
	}

	function makeRequest($urlAppendix) {
		ApiLoader::$CURRENT_REQUST_NUMBER++;
		while (true) {
			$content = file_get_contents(ApiLoader::BASE_URL.$urlAppendix);
			if ($content == false) {
				$sleepTime = 30;
				echo "max requests per minutes reached.\n";
				echo "sleep for ".$sleepTime." seconds, than retry\n";
				echo "request number: ".ApiLoader::$CURRENT_REQUST_NUMBER."\n\n";
				sleep($sleepTime);
			} else {
				return $content;
			}
		}
	}
}