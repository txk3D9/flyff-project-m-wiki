#https://www.mediawiki.org/wiki/Special:ExtensionDistributor


#@todo those downloads dont work anymore :(
declare -a arr=("AdminLinks-REL1_36-7d79678" "Cargo-REL1_36-03fffb9" "ConfirmEdit-REL1_36-d1c624a"
"NumberFormat-REL1_36-ef44cf4"
"PageForms-REL1_36-290eccb" "ParserFunctions-REL1_36-67118a5" "ReplaceText-REL1_36-9150470"
"SecureLinkFixer-REL1_36-b985af4" "TinyMCE-REL1_36-3b5d214" "TitleKey-REL1_36-f7e79a5" "Variables-REL1_36-6d040fb"
"VisualEditor-REL1_36-c9fc510" "WikiEditor-REL1_36-5ef2d32")

for i in "${arr[@]}"; do
  wget https://extdist.wmflabs.org/dist/extensions/$i.tar.gz
  tar -xzf $i.tar.gz -C ../html/extensions
  rm $i.tar.gz
done
